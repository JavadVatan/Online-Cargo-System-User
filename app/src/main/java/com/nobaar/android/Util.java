package com.nobaar.android;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;

import com.nobaar.android.global.Constants;
import com.nobaar.android.global.ConvertDate;
import com.nobaar.android.struct.StructDate;

import java.io.IOException;
import java.util.Calendar;
import java.util.StringTokenizer;

/**
 * Created by Javad Vatan on 10/27/2017.
 */

public class Util {
    public static String getDecimalFormattedString(String value) {
        try {
            StringTokenizer lst = new StringTokenizer(value, ".");
            String str1 = value;
            String str2 = "";
            if (lst.countTokens() > 1) {
                str1 = lst.nextToken();
                str2 = lst.nextToken();
            }
            String str3 = "";
            int i = 0;
            int j = -1 + str1.length();
            if (str1.charAt(-1 + str1.length()) == '.') {
                j--;
                str3 = ".";
            }
            for (int k = j; ; k--) {
                if (k < 0) {
                    if (str2.length() > 0)
                        str3 = str3 + "." + str2;
                    return str3;
                }
                if (i == 3) {
                    str3 = "," + str3;
                    i = 0;
                }
                str3 = str1.charAt(k) + str3;
                i++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return value;
    }

    public Bitmap modifyOrientation(Activity mActivity, Bitmap bitmap, Uri dataImage, String image_absolute_path) throws IOException {
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        if (orientation == ExifInterface.ORIENTATION_UNDEFINED) {
            try {
                String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};
                Cursor cursor = mActivity.getContentResolver().query(dataImage, projection,
                        null, null, null);
                if (cursor.moveToFirst()) {
                    orientation = cursor.getInt(0);
                }
                cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
            case 90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
            case 180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
            case 270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }

    }

    private Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public long convertDateAndTimeToMilliSec(Context mContext, StructDate structDate) {
        long timeInMilliseconds = Constants.NO_DATA;
        Calendar calendar = Calendar.getInstance();
        ConvertDate convertdate = ConvertDate.getInstance();

        convertdate.calcPersian(structDate.getYear(), structDate.getMonth(), structDate.getDay());
        int[] date = convertdate.GetGregorianDate();
        calendar.set(date[0], date[1], date[2], structDate.getHourInDay(), structDate.getMinute());
        timeInMilliseconds = calendar.getTimeInMillis();
        return timeInMilliseconds;
    }
    /*
    *     convertdate.calcPersian(structDate.getYear(), structDate.getMonth()-1, structDate.getDay());
        int[] date = convertdate.GetGregorianDate();
        */
}
