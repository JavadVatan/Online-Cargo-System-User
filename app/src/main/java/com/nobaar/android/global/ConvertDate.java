package com.nobaar.android.global;

public class ConvertDate {

	/*
     * JavaScript privates for the Fourmilab Calendar Converter
	 * 
	 * by John Walker -- September, MIM
	 * http://www.fourmilab.ch/documents/calendar/
	 * 
	 * This program is in the public domain.
	 */

    /*
     * You may notice that a variety of array variables logically local to
     * privates are declared globally here. In JavaScript, construction of an
     * array variable from source code occurs as the code is interpreted. Making
     * these variables pseudo-globals permits us to avoid overhead constructing
     * and disposing of them in each call on the private in which whey are used.
     */
    private static final double GREGORIAN_EPOCH = 1721425.5;
    private static final double ISLAMIC_EPOCH = 1948439.5;
    private static final double PERSIAN_EPOCH = 1948320.5;
    private static ConvertDate myInstance;

    // *****************************************************************************
    private double JulianDate;

    private ConvertDate() {

    }

    public static ConvertDate getInstance() {
        if (myInstance == null) {
            myInstance = new ConvertDate();
        }
        return myInstance;
    }

    /*
     * MOD -- Modulus function which works for non-integers.
     */
    private double mod(double a, double b) {
        return a - (b * Math.floor(a / b));
    }

    // LEAP_GREGORIAN -- Is a given year in the Gregorian calendar a leap year ?
    public boolean leap_gregorian(double year) {
        return ((year % 4.0) == 0.0) && (!(((year % 100.0) == 0.0) && ((year % 400.0) != 0.0)));
    }

    // LEAP_ISLAMIC -- Is a given year a leap year in the Islamic calendar ?
    public boolean leap_islamic(double year) {
        return (((year * 11.0) + 14.0) % 30.0) < 11.0;
    }

    // LEAP_PERSIAN -- Is a given year a leap year in the Persian calendar ?
    public boolean leap_persian(double year) {
        return ((((((year - ((year > 0) ? 474 : 473)) % 2820) + 474) + 38) * 682) % 2816) < 682;
    }

    // GREGORIAN_TO_JD -- Determine Julian day number from Gregorian calendar
    // date

    private double gregorian_to_jd(double year, double month, double day) {
        return (GREGORIAN_EPOCH - 1.0) + (365.0 * (year - 1.0)) + Math.floor((year - 1.0) / 4.0) +
                (-Math.floor((year - 1.0) / 100.0)) + Math.floor((year - 1.0) / 400.0) + Math.floor(
                (((367.0 * month) - 362.0) / 12.0) + ((month <= 2.0) ? 0.0 : (leap_gregorian(year)
                        ? -1.0 : -2.0)) + day);
    }

    // ISLAMIC_TO_JD -- Determine Julian day from Islamic date

    private double islamic_to_jd(double year, double month, double day) {
        return (day + Math.ceil(29.5 * (month - 1.0)) + (year - 1.0) * 354.0 + Math.floor(
                (3.0 + (11.0 * year)) / 30.0) + ISLAMIC_EPOCH) - 1.0;
    }

    // PERSIAN_TO_JD -- Determine Julian day from Persian date

    private double persian_to_jd(double year, double month, double day) {
        double epbase, epyear;

        epbase = year - ((year >= 0) ? 474.0 : 473.0);
        epyear = 474.0 + mod(epbase, 2820.0);

        return day + ((month <= 7.0) ? ((month - 1.0) * 31.0) : (((month - 1.0) * 30.0) + 6.0)) +
                Math.floor(((epyear * 682.0) - 110.0) / 2816.0) + (epyear - 1.0) * 365.0 +
                Math.floor(epbase / 2820.0) * 1029983.0 + (PERSIAN_EPOCH - 1.0);
    }

    // JD_TO_ISLAMIC -- Calculate Islamic date from Julian day
    private double[] jd_to_islamic(double jd) {
        double year, month, day;
        double result[] = {2010, 1, 1};
        jd = Math.floor(jd) + 0.5;
        year = Math.floor(((30.0 * (jd - ISLAMIC_EPOCH)) + 10646.0) / 10631.0);
        month = Math.min(12.0, Math.ceil((jd - (29.0 + islamic_to_jd(year, 1.0, 1.0))) / 29.5) +
                1.0);
        day = (jd - islamic_to_jd(year, month, 1.0)) + 1.0;
        result[0] = year;
        result[1] = month;
        result[2] = day;

        return result;
    }

    // JD_TO_PERSIAN -- Calculate Persian date from Julian day
    private double[] jd_to_persian(double jd) {
        double year, month, day, depoch, cycle, cyear, ycycle, aux1, aux2, yday;
        double result[] = {1390, 1, 1};

        jd = Math.floor(jd) + 0.5;

        depoch = jd - persian_to_jd(475.0, 1.0, 1.0);
        cycle = Math.floor(depoch / 1029983.0);
        cyear = mod(depoch, 1029983.0);
        if (cyear == 1029982.0) {
            ycycle = 2820.0;
        } else {
            aux1 = Math.floor(cyear / 366.0);
            aux2 = mod(cyear, 366.0);
            ycycle = Math.floor(((2134.0 * aux1) + (2816.0 * aux2) + 2815.0) / 1028522.0) + aux1 +
                    1.0;
        }
        year = ycycle + (2820.0 * cycle) + 474.0;
        if (year <= 0) {
            year--;
        }
        yday = (jd - persian_to_jd(year, 1.0, 1.0)) + 1.0;
        month = (yday <= 186.0) ? Math.ceil(yday / 31.0) : Math.ceil((yday - 6.0) / 30.0);
        day = (jd - persian_to_jd(year, month, 1.0)) + 1.0;
        result[0] = year;
        result[1] = month;
        result[2] = day;
        return result;
    }

    // JD_TO_GREGORIAN -- Calculate Gregorian calendar date from Julian day

    private double[] jd_to_gregorian(double jd) {
        double wjd, depoch, quadricent, dqc, cent, dcent, quad, dquad, yindex, year, month, day,
                yearday, leapadj;
        double result[] = {2010, 1, 1};

        wjd = Math.floor(jd - 0.5) + 0.5;
        depoch = wjd - GREGORIAN_EPOCH;
        quadricent = Math.floor(depoch / 146097.0);
        dqc = mod(depoch, 146097.0);
        cent = Math.floor(dqc / 36524.0);
        dcent = mod(dqc, 36524.0);
        quad = Math.floor(dcent / 1461.0);
        dquad = mod(dcent, 1461.0);
        yindex = Math.floor(dquad / 365.0);
        year = (quadricent * 400.0) + (cent * 100.0) + (quad * 4.0) + yindex;
        if (!((cent == 4.0) || (yindex == 4.0))) {
            year++;
        }
        yearday = wjd - gregorian_to_jd(year, 1.0, 1.0);
        leapadj = ((wjd < gregorian_to_jd(year, 3.0, 1.0)) ? 0.0 : (leap_gregorian(year) ? 1.0
                : 2.0));
        month = Math.floor((((yearday + leapadj) * 12.0) + 373.0) / 367.0);
        day = (wjd - gregorian_to_jd(year, month, 1.0)) + 1.0;

        result[0] = year;
        result[1] = month;
        result[2] = day;
        return result;
    }

    /*
     * updateFromGregorian -- Update all calendars from Gregorian. "Why not
     * Julian date?" you ask. Because starting from Gregorian guarantees we're
     * already snapped to an integral second, so we don't get roundoff errors in
     * other calendars.
     */
    private void updateFromGregorian(int _year, int _month, int _day) {
        double year, mon, mday, hour, min, sec;
        year = _year;
        if (year < 1000.0) {
            year += 1900.0;
        }

        mon = _month;
        mday = _day;
        hour = min = sec = 0;

        // Update Julian day

        JulianDate = gregorian_to_jd(year, mon, mday) + ((Math.floor(
                sec + 60.0 * (min + 60.0 * hour) + 0.5)) / 86400.0);

    }

    // calcGregorian -- Perform calculation starting with a Gregorian date
    public void calcGregorian(int _year, int _month, int _day) {
        updateFromGregorian(_year, _month, _day);
    }

    // calcIslamic -- Update from Islamic calendar

    public void calcIslamic(int _year, int _month, int _day) {
        JulianDate = islamic_to_jd(_year, _month, _day);
    }

    // calcPersian -- Update from Persian calendar

    public void calcPersian(int _year, int _month, int _day) {
        JulianDate = persian_to_jd(_year, _month, _day);
    }

    public int[] GetGregorianDate() {
        int gregorianDate[] = {2000, 1, 1};
        double[] gregcal;
        // Update gregorian Calendar
        gregcal = jd_to_gregorian(JulianDate);
        gregorianDate[0] = (int) gregcal[0];
        gregorianDate[1] = (int) gregcal[1];
        gregorianDate[2] = (int) gregcal[2];
        return gregorianDate;
    }

    public int[] GetPersianDate() {
        int persianDate[] = {1300, 1, 1};
        double[] perscal;
        // Update Persian Calendar
        perscal = jd_to_persian(JulianDate);
        persianDate[0] = (int) perscal[0];
        persianDate[1] = (int) perscal[1];
        persianDate[2] = (int) perscal[2];
        return persianDate;
    }

    public int[] GetLunaryDate() {
        int IslamicDate[] = {1400, 1, 1};
        double[] islcal;
        // Update Islamic Calendar
        islcal = jd_to_islamic(JulianDate);
        IslamicDate[0] = (int) (islcal[0]);
        IslamicDate[1] = (int) (islcal[1]);
        IslamicDate[2] = (int) (islcal[2]);
        return IslamicDate;
    }

}
