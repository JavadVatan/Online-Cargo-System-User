package com.nobaar.android.global;

import android.graphics.Typeface;

public class Constants {

    public static final String BASE_URL = "http://nobaar.com/apibar/public/";

    public static final String API_LOGIN = BASE_URL + "send_mobile/";
    public static final String API_ACTIVATION = BASE_URL + "send_activeCode/";
    public static final String API_SEND_PROFILE = BASE_URL + "send_profile";
    public static final String API_UPLOAD_PROFILE = BASE_URL + "upload_profile";
    public static final String API_GET_MESSAGE = BASE_URL + "get_message";
    public static final String API_GET_ORDER = BASE_URL + "get_order/";
    public static final String API_SEND_MESSAGE = BASE_URL + "send_message";
    public static final String API_GET_CAR = BASE_URL + "get_car";
    public static final String API_GET_EXPENSIVE_TOOL = BASE_URL + "get_vasayeleHazinedar";
    public static final String API_SEND_ORDER = BASE_URL + "send_order";
    public static final String API_ACCEPT_ORDER = BASE_URL + "accept_order";
    public static final String API_GET_ORDER_STATUS = BASE_URL + "get_order_status/";
    public static final String API_CANCEL_ORDER = BASE_URL + "cancel_order";
    public static final String API_UPDATE_ORDER_DRIVER_STATUS = BASE_URL + "update_order_driver_status";
    public static final String API_GET_ORDER_BY_ORDER_ID = BASE_URL + "get_order_by_order_id/";
    public static final String API_GET_CHECK_DISCOUNT_CODE = BASE_URL + "check_discount_code";
    public static final String API_ADD_RANK = BASE_URL + "add_rank";
    public static final String API_SEND_TO_BANK = BASE_URL + "send_to_bank";
    public static final String API_BANK = "http://www.nobaar.com/apibar/direct.php?result=";
    public static final String API_GET_SHARE_CODE = BASE_URL + "get_share_code/";
    public static final String API_GET_CAL_TIME = BASE_URL + "get_calc_time";
    public static final String API_SEND_TO_BANK_WALLET = BASE_URL + "send_to_bank_wallet";
    public static final String API_SEND_GET_CREDIT= BASE_URL + "get_wallet";


    public static final String JSON_STATUS = "status";
    public static final String JSON_DATA = "data";
    public static final String JSON_STATUS_200 = "200";
    public static final String JSON_STATUS_201 = "201";
    public static final String JSON_STATUS_401 = "401";
    public static final String JSON_STATUS_402 = "402";
    public static final String JSON_MESSAGE = "message";
    public static final String JSON_TITLE = "title";
    public static final String JSON_BODY = "body";
    public static final String JSON_BODY_LOWER_CASE = "body";
    public static final String JSON_DATE = "date";
    public static final String JSON_USER_NOT_FOUND = "UserNotFound";
    public static final String JSON_ERROR = "error";
    public static final String JSON_ENTER_FULL_NAME = "EnterFullname";
    public static final String JSON_PHONE_NOT_VALID = "PhoneNotValid";
    public static final String JSON_EMAIL_NOT_VALID = "EmailNotValid";
    public static final String JSON_PASSWORD_NOT_VALID = "PasswordNotValid";
    public static final String JSON_PASSWORD_NOT_CORRECT = "PasswordNotCorrect";
    public static final String JSON_ENTER_ADDRESS = "EnterAddress";
    public static final String JSON_USER_EXIST = "UserExist";
    public static final String JSON_ORDER_ID = "order_id";
    public static final String JSON_USER_ID_LOWER_CASE = "user_id";
    public static final String JSON_START_ADDRESS_LAT = "startAddressLat";
    public static final String JSON_START_ADDRESS_LONG = "startAddressLong";
    public static final String JSON_START_ADDRESS = "startAddress";
    public static final String JSON_END_ADDRESS_LAT = "endAddressLat";
    public static final String JSON_END_ADDRESS_LONG = "endAddressLong";
    public static final String JSON_END_ADDRESS = "endAddress";
    public static final String JSON_PAY_WITH_RECIVIER = "pay_with_receiver";
    public static final String JSON_RECIVIER_NAME = "receiver_name";
    public static final String JSON_RECIVIER_MOBILE = "receiver_mobile";
    public static final String JSON_TIME = "time";
    public static final String JSON_BARBAR_WORKER = "barbar_worker";
    public static final String JSON_CHIDEMAN_WORKER = "chideman_worker";
    public static final String JSON_ORIGIN_FLOOR = "origin_floor";
    public static final String JSON_ORIGIN_WALKING = "origin_walking";
    public static final String JSON_DESTINATION_FLOOR = "destination_floor";
    public static final String JSON_DESTINATION_WALKING = "destination_walking";
    public static final String JSON_VASAYEL_HAZINEDAR = "vasayeleHazinedar";
    public static final String JSON_CAR_ID = "car_id";
    public static final String JSON_NAME = "name";
    public static final String JSON_FAMILY = "family";
    public static final String JSON_MONEY_BAG = "moneyBag";
    public static final String JSON_HAVE_TRAVEL = "haveTravel";
    public static final String JSON_PASSWORD = "password";
    public static final String JSON_OLD_PASSWORD = "oldpassword";
    public static final String JSON_MOBILE = "mobile";
    public static final String JSON_EMAIL = "email";
    public static final String JSON_CODE = "code";
    public static final String JSON_PRICE = "price";
    public static final String JSON_IS_FIRST = "is_first";
    public static final String JSON_USER_PHONE = "user_phone";
    public static final String JSON_USER_COST = "user_cost";
    public static final String JSON_USER_FULL_NAME = "user_fullname";
    public static final String JSON_COST = "cost";
    public static final String JSON_DISCOUNT_CODE = "code";
    public static final String JSON_ADDRESS = "address";
    public static final String JSON_ID = "id";
    public static final String JSON_COUNT = "count";
    public static final String JSON_SUBJECT = "subject";
    public static final String JSON_PICTURE_ENABLE = "picture_enable";
    public static final String JSON_PICTURE_DISABLE = "picture_disable";
    public static final String JSON_DRIVER = "driver";
    public static final String JSON_DRIVER_NAME = "driver_name";
    public static final String JSON_DRIVER_FAMILY = "driver_family";
    public static final String JSON_DRIVER_MOBILE = "driver_mobile";
    public static final String JSON_DRIVER_CAR = "driver_car";
    public static final String JSON_DRIVER_CAR_PIC = "driver_pic";
    public static final String JSON_DRIVER_RANK = "driver_rank";
    public static final String JSON_IMAGE = "image";
    public static final String JSON_BON = "bon";
    public static final String JSON_TYPE = "type";
    public static final String JSON_AMOUNT = "amount";
    public static final String JSON_TOKEN = "token";
    public static final String JSON_DRIVER_ID = "driver_id";
    public static final String JSON_RANK = "rank";
    public static final String JSON_HAS_DATA = "1";
    public static final String JSON_SHARE_CODE = "share_code";
    public static final String JSON_TIME_CAL = "timeCalc";
    public static final String PAYMENT_IS_READY = "0";


    public static final int LOCATION_REFRESH_TIME = 2;
    public static final float LOCATION_MIN_DISTANCE_CHANGE_FOR_UPDATES = 5f;
    public static final String BROAD_CAST_STATE_GPS_OFF = "com.vallcoSofwereGroup.nobar.global_GPS_off";
    public static final String BROAD_CAST_STATE_GPS_ON = "com.vallcoSofwereGroup.nobar.global_GPS_on";
    /**
     *
     */
    public static final int NO_DATA = -1;
    public static final String Preferences_USER_LOGIN = "user_login";
    public static final String Preferences_USER_NAME = "user_name";
    public static final String Preferences_USER_FAMILY = "user_family";
    public static final String Preferences_USER_ID = "user_id";
    public static final String Preferences_IS_FIRST = "is_first";
    public static final String Preferences_USER_PHONE = "user_phone";
    public static final String Preferences_USER_CREDIT = "user_cost";
    public static final String Preferences_USER_EMAIL = "user_email";
    public static final String Preferences_USER_PHOTO_URL = "user_photo_url";
    /**
     * Image Profile
     */
    public static final String IMAGE_SAVE_NAME_PROFILE = "nobar_user_image.png";
    public static final String IMAGE_SAVE_FOLDER = "nobar";
    public static final int IMAGE_SIZE = 250;
    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    /**
     * Order Status
     */
    public static final int STATUS_DRIVER_CANCEL_ORDER = 6;
    public static final int STATUS_DRIVER_ACCEPT_ORDER = 2;
    public static final int STATUS_DRIVER_ACCEPT_DETAIL_ORDER = 3;
    public static final int STATUS_DRIVER_START_PROCESS = 4;
    public static final int STATUS_DRIVER_END_PROCESS = 5;
    public static final int STATUS_DRIVER_FINISHED_ORDER = 9;
    public static final int STATUS_ORDER_REGISTERED = 1;
    /**
     * Location Property
     */
    public static Typeface iranSenseLight;
    public static Typeface iranSenseBold;
}

