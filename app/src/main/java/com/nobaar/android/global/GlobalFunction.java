package com.nobaar.android.global;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.nobaar.android.R;

import java.io.File;

import static android.content.Context.CLIPBOARD_SERVICE;

public class GlobalFunction {
    private static GlobalFunction myInstance;

    public GlobalFunction() {
    }

    public static synchronized GlobalFunction getInstance() {
        if (myInstance == null) {
            myInstance = new GlobalFunction();
        }
        return myInstance;
    }

    public void openFile(Context mcontext, String fileName) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(fileName)),
                "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mcontext.startActivity(intent);
    }

    public String getAppName(Context context) {
        PackageManager packagemanager;
        packagemanager = context.getPackageManager();
        try {
            ApplicationInfo applicationinfo = packagemanager.getApplicationInfo(
                    context.getPackageName(), 0);
            return packagemanager.getApplicationLabel(applicationinfo).toString();
        } catch (Exception e) {
            return "can't get app name";
        }
    }

    @SuppressLint("NewApi")
    public String getDeviceSerial(Context mContext) {
        String serial = "~#";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD)
            serial = android.os.Build.SERIAL;
        return serial;
    }

    public String getAndroidId(Context mContext) {
        String id = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return id;
    }

    public boolean isGpsOn(Context mContext) {
        LocationManager mLocationManager = (LocationManager) mContext
                .getSystemService(Context.LOCATION_SERVICE);

        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void turnGPSOn(Activity activity) {
        String provider = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!provider.contains("gps")) { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            activity.sendBroadcast(poke);
        }
    }

    public void sendBroadcastStateGpsOff(Context mContext) {
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(
                new Intent(Constants.BROAD_CAST_STATE_GPS_OFF));
    }

    public void sendBroadcastStateGpsOn(Context mContext) {
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(
                new Intent(Constants.BROAD_CAST_STATE_GPS_ON));
    }

    public void shareText(Context mcontext, String text, String title) {
        Intent intent;
        intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if (mcontext.getPackageManager().resolveActivity(intent,
                PackageManager.MATCH_DEFAULT_ONLY) != null)
            mcontext.startActivity(intent);
        mcontext.startActivity(Intent.createChooser(intent, title));
    }

    public void deleteFile(String fileName) {
        File file = new File(fileName);
        file.delete();
    }

    public void copyTextIntoCliboard(Context mContext, String label, String text) {
        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(label, text);
        clipboard.setPrimaryClip(clip);
    }

    public void openBrowser(Context mContext, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mContext.startActivity(intent);
    }

    public void openNetworkSetting(Context mContext) {
        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
        mContext.startActivity(intent);
    }

    public void openGPSSetting(Context mContext) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        mContext.startActivity(intent);
    }

    public void overrideFonts(final Context context, final View v) {
        if (Constants.iranSenseLight == null) {
            Constants.iranSenseLight = Typeface.createFromAsset(context.getAssets(), "IRANSans.ttf");
        }
        if (Constants.iranSenseBold == null) {
            Constants.iranSenseBold = Typeface.createFromAsset(context.getAssets(), "IRANSansBold.ttf");
        }
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(Constants.iranSenseLight);
            }
        } catch (Exception e) {
        }
    }

    public void changeStatusBarColor(Activity activity, int colorId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(activity.getResources().getColor(colorId));
        }
    }

    public int getWidthScreenSize(Context mContext) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        return displayMetrics.widthPixels;
    }

    public int geHeightScreenSize(Context mContext) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        return displayMetrics.heightPixels;
    }

    public void toast(Context mContext, String text) {
        Activity activity = (Activity) mContext;
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast, null);

        TextView text_widget = layout.findViewById(R.id.toast_text);
        text_widget.setText(text);
        text_widget.setTypeface(Constants.iranSenseLight);

        Toast toast = new Toast(mContext);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    public void openKeyboard(Activity activity) {
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInputFromWindow(
                    currentFocusedView.getApplicationWindowToken(),
                    InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public void closeKeyboard(Activity activity) {
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), 0);
        }
    }
      /*  if (currentFocusedView != null) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
*/
}
