package com.nobaar.android.global;

import android.content.Context;
import android.content.SharedPreferences;

import com.nobaar.android.Application;

/**
 * Created by Javad Vatan on 9/5/2017.
 */

public class TemporaryPreferences {

    public static final String PREF_ADDRESS_ORIGIN = "start_address";
    public static final String PREF_PAY_WITH_RECEIVER = "pay_with_reciver ";
    public static final String PREF_RECEIVER_NAME = "reciver_name";
    public static final String PREF_RECEIVER_MOBILE = "Reciver_mobile";
    public static final String PREF_TIME_MILLISECOND = "Time_millisecond";
    public static final String PREF_TIME_STRING = "Time_string";
    public static final String PREF_ORIGIN_FLOOR = "origin_floor";
    public static final String PREF_DESTINATION_FLOOR = "destination_floor";
    public static final String PREF_ORIGIN_WALKING = "origin_walking";
    public static final String PREF_DESTINATION_WALKING = "destination_walking";
    public static final String PREF_VASAYEL_HAZINEHDAR = "vasayeleHazinedar ";
    public static final String PREF_CAR_ID = "Car_id  ";
    public static final String PREF_ORDER_ID = "order_id";
    public static final String PREF_ORDER_PRICE = "order_price";
    public static final String PREF_IS_ACTIVE_ORDER = "is_active_order";
    public static final String PREF_DRIVER_NAME = "driver_name";
    public static final String PREF_DRIVER_CAR_NAME = "driver_car_name";
    public static final String PREF_DRIVER_PIC = "diver_pic";
    public static final String PREF_DRIVER_ID = "diver_id";
    public static final String PREF_BANK_REQUEST = "bank_request";
    public static final String PREF_BANK_REQUEST_WALLET = "bank_request_wallet";
    public static final String PREF_BANK_REQUEST_PAY_COST = "bank_request_pay_cost";

    public static final String PREF_KARGAR_BAR = "kargar_bar";
    public static final String PREF_KARGAR_CHIDEMAN = "chideman";
    public static final String PREF_ADDRESS_DESTINATION = "end_address";
    private static final String Preferences_ORIGIN_LAT = "origin_lat";
    private static final String Preferences_ORIGIN_LONG = "origin_long";
    private static final String Preferences_DESTINATION_LAT = "destination_lat";
    private static final String Preferences_DESTINATION_LONG = "destination_long";
    private static TemporaryPreferences myInstance;
    private Context mContext;
    private SharedPreferences mSharedPreferences;

    public TemporaryPreferences() {
        this.mContext = Application.getInstance().getContext();
        mSharedPreferences = mContext.getSharedPreferences(
                "temporaryPreference", Context.MODE_PRIVATE);
    }

    public static TemporaryPreferences getInstance() {
        if (myInstance == null) {
            myInstance = new TemporaryPreferences();
        }
        return myInstance;
    }

    public double getOriginLat() {
        return Double.valueOf(mSharedPreferences.getString(Preferences_ORIGIN_LAT, "1"));
    }

    public void setOriginLat(double lat) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Preferences_ORIGIN_LAT, String.valueOf(lat));
        editor.apply();
    }

    public double getOriginLong() {
        return Double.valueOf(mSharedPreferences.getString(Preferences_ORIGIN_LONG, "1"));
    }

    public void setOriginLong(double lng) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Preferences_ORIGIN_LONG, String.valueOf(lng));
        editor.apply();
    }

    public double getDestinationLat() {
        return Double.valueOf(mSharedPreferences.getString(Preferences_DESTINATION_LAT, "0"));
    }

    public void setDestinationLat(double lat) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Preferences_DESTINATION_LAT, String.valueOf(lat));
        editor.apply();
    }

    public double getDestinationLong() {
        return Double.valueOf(mSharedPreferences.getString(Preferences_DESTINATION_LONG, "0"));
    }

    public void setDestinationLong(double lng) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Preferences_DESTINATION_LONG, String.valueOf(lng));
        editor.apply();
    }

    public void addPrefInt(String key, int value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public int getPrefInt(String key, int def) {
        return mSharedPreferences.getInt(key, def);
    }

    public void addPrefString(String key, String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getPrefString(String key, String def) {
        return mSharedPreferences.getString(key, def);
    }

    public void addPreBool(String key, boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getPrefBool(String key, boolean def) {
        return mSharedPreferences.getBoolean(key, def);
    }

    public void clearPreferences() {
        mSharedPreferences.edit().clear().apply();
    }
}
