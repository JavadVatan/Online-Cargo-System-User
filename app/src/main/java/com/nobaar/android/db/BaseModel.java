package com.nobaar.android.db;


import com.nobaar.android.Application;

public abstract class BaseModel {

    protected Application app = Application.getInstance();

    public abstract long insert();

    public abstract boolean update();

    public abstract boolean delete();
}
