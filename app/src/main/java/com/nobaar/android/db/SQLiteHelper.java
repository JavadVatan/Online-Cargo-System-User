package com.nobaar.android.db;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.nobaar.android.Application;


/**
 * Created by MikroCom on 22/08/2017.
 */

public class SQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "nobar.com";
    private static final int DATABASE_VERSION = 1;

    /**
     * Table Names
     */
    public static final String TABLE_BOOK_MARK = "book_mark";

    /**
     * Global Field Names
     */
    public static final String COL_ID = "_id";


    /**
     * Bookmark Field Names
     */
    public static final String BOOK_MARK_ADDRESS = "address";
    public static final String BOOK_MARK_LAT = "latitude";
    public static final String BOOK_MARK_LONG = "longitude";

    /**
     * Create Tables
     */

    private static final String CREATE_BOOK_MARK_TABLE = "create table if not exists "
            + TABLE_BOOK_MARK + "("
            + COL_ID + " integer primary key autoincrement, "
            + BOOK_MARK_ADDRESS + " text, "
            + BOOK_MARK_LAT + " text, "
            + BOOK_MARK_LONG + " text);";


    public SQLiteHelper() {
        super(Application.getInstance().getContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_BOOK_MARK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
