package com.nobaar.android.db;

import android.content.ContentValues;
import android.database.Cursor;

import com.nobaar.android.Application;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MikroCom on 20/07/2017.
 */

public class StructAddressHelper extends BaseModel {

    private int id;
    private String address;
    private String latitude;
    private String longitude;

    public StructAddressHelper() {
    }

    public StructAddressHelper(int id, String address, String latitude, String longitude) {
        this.id = id;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public StructAddressHelper(String address, String latitude, String longitude) {
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public static List<StructAddressHelper> getAll() {
        List<StructAddressHelper> dataList = new ArrayList<>();

        Cursor mCursor = Application.getInstance().mDbHelper.getReadableDatabase()
                .rawQuery("select * from " + SQLiteHelper.TABLE_BOOK_MARK, null);

        if (mCursor != null && mCursor.getCount() > 0) {
            mCursor.moveToFirst();
            while (mCursor.moveToNext()) {
                StructAddressHelper structAddressHelper = new StructAddressHelper();

                structAddressHelper.setId(mCursor.getInt(mCursor.getColumnIndex(SQLiteHelper.COL_ID)));
                structAddressHelper.setAddress(mCursor.getString(mCursor.getColumnIndex(SQLiteHelper.BOOK_MARK_ADDRESS)));
                structAddressHelper.setLatitude(mCursor.getString(mCursor.getColumnIndex(SQLiteHelper.BOOK_MARK_LAT)));
                structAddressHelper.setLongitude(mCursor.getString(mCursor.getColumnIndex(SQLiteHelper.BOOK_MARK_LONG)));

                dataList.add(structAddressHelper);
            }
        }

        return dataList;
    }

    public static void insertAll(List<StructAddressHelper> dataList) {
        for (StructAddressHelper structAddressHelper : dataList) {
            structAddressHelper.insert();
        }
    }

    @Override
    public long insert() {
        ContentValues cv = new ContentValues();
        cv.put(SQLiteHelper.BOOK_MARK_ADDRESS, address);
        cv.put(SQLiteHelper.BOOK_MARK_LAT, latitude);
        cv.put(SQLiteHelper.BOOK_MARK_LONG, longitude);

        return app.mDbHelper.getWritableDatabase().insert(SQLiteHelper.TABLE_BOOK_MARK, null, cv);
    }

    @Override
    public boolean update() {
        ContentValues cv = new ContentValues();
        cv.put(SQLiteHelper.BOOK_MARK_ADDRESS, address);
        cv.put(SQLiteHelper.BOOK_MARK_LAT, latitude);
        cv.put(SQLiteHelper.BOOK_MARK_LONG, longitude);

        String where = SQLiteHelper.COL_ID + "=" + id;

        return app.mDbHelper.getWritableDatabase().update(SQLiteHelper.TABLE_BOOK_MARK, cv, where, null) > 0;
    }

    @Override
    public boolean delete() {
        String where = SQLiteHelper.COL_ID + "=" + id;

        return app.mDbHelper.getWritableDatabase().delete(SQLiteHelper.TABLE_BOOK_MARK, where, null) > 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
