package com.nobaar.android.network;

/**
 * Created by MikroCom on 12/07/2017.
 */

public class StructKeyValue {

    private String key;
    private String value;

    public StructKeyValue(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
