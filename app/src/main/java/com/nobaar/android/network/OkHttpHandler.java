package com.nobaar.android.network;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.nobaar.android.Application;
import com.nobaar.android.activity.NoInternetConnectionActivity;
import com.nobaar.android.global.Constants;
//import com.readystatesoftware.chuck.ChuckInterceptor;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class OkHttpHandler extends AsyncTask<String, Void, String> {
    private static final int TIME_OUT_SEND_FILE = 300;
    private static OkHttpClient clientInstance;
    public final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

    private RequestBody jsonRequestBody = null, multipartRequestBody = null;
    private FormBody.Builder formBuilder;
    private List<StructKeyValue> headers;
    private String url;
    private boolean canShowErrorDialog = true;
    private boolean isPostRequest = false;
    private NetworkEventListener mListener;
    private Context mContext;

    public OkHttpHandler(Context mContext, String url, NetworkEventListener mListener) {
        this.url = url;
        this.mListener = mListener;
        this.formBuilder = new FormBody.Builder();
        this.headers = new ArrayList<>();
        this.mContext = mContext;
        if (clientInstance == null)
            clientInstance = new OkHttpClient.Builder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(50, TimeUnit.SECONDS)
//                   .addInterceptor(new ChuckInterceptor(mContext))
                    .build();
    }

    public OkHttpHandler addHeader(String key, String value) {
        headers.add(new StructKeyValue(key, value == null ? "" : value));
        return this;
    }

    public OkHttpHandler addParam(String key, String value) {
        value = value == null ? "" : value;
        formBuilder.add(key, value);
        isPostRequest = true;
        return this;
    }

    public OkHttpHandler addImage(File file, String key, String value) {

        clientInstance = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT_SEND_FILE, TimeUnit.SECONDS)
                .readTimeout(TIME_OUT_SEND_FILE, TimeUnit.SECONDS)
//                .addInterceptor(new ChuckInterceptor(mContext))
                .build();

        multipartRequestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart(Constants.JSON_IMAGE, file.getName(), RequestBody.create(
                        MEDIA_TYPE_PNG, file))
                .addFormDataPart(key, value)
                .build();

        return this;
    }

    public OkHttpHandler addParam(FormBody.Builder formBuilder) {
        this.formBuilder = formBuilder;
        isPostRequest = true;

        return this;
    }

    public OkHttpHandler showErrorInternet(boolean canShowErrorDialog) {
        this.canShowErrorDialog = canShowErrorDialog;
        return this;
    }

    public OkHttpHandler setSpecialBody(MediaType mediaType, String data) {
        data = data == null ? "" : data;
        jsonRequestBody = RequestBody.create(mediaType, data);
        return this;
    }

    public OkHttpHandler setDiscriptionErrorMessage(String errorMessage) {
        return this;
    }


    public void send() {
        if (Application.getInstance().isConnectionAnyInternet())
            execute();
        else if (canShowErrorDialog) {
            //showDialog();
            manageIntentConnection();
            sendCustomError("");
        }
    }

    private void manageIntentConnection() {
        Application.getInstance().getContext().startActivity(new Intent
                (Application.getInstance().getContext(), NoInternetConnectionActivity.class));
    }

    @Override
    protected String doInBackground(String... params) {
        String res = null;
        RequestBody body;


        if (jsonRequestBody != null)
            body = jsonRequestBody;
        else if (multipartRequestBody != null)
            body = multipartRequestBody;
        else
            body = formBuilder.build();

        Request.Builder request;
        if (isPostRequest)
            request = new Request.Builder().url(url).post(body);
        else
            request = new Request.Builder().url(url).get();

        for (StructKeyValue kv : headers) {
            request.addHeader(kv.getKey(), kv.getValue());
        }

        Response response;
        try {
            response = clientInstance.newCall(request.build()).execute();
            res = response.body().string();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return res;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        JSONObject objResponse = null;
        try {
            objResponse = new JSONObject(s);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objResponse == null && s != null) {
            sendCustomError("خطای غیر منتظر رخ داده است. کد خطا 101");

        } else {
            try {
                if (objResponse.has(Constants.JSON_STATUS))
                    mListener.onSuccess(objResponse);

            } catch (Exception e) {
                sendCustomError("خطای غیر منتظر رخ داده است. کد خطا 102");
                e.printStackTrace();
            }
        }
    }

    private void sendCustomError(String errorMessage) {
        //    JSONObject jsonObject = new JSONObject();
        //     try {
/*            jsonObject.put(Constants.JSON_CODE, "");
            jsonObject.put(Constants.JSON_MESSAGE, errorMessage);*/
        mListener.onError(errorMessage);
  /*      } catch (JSONException e1) {
            e1.printStackTrace();
        }*/
    }

}
