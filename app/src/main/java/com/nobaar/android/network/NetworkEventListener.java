package com.nobaar.android.network;

import org.json.JSONException;
import org.json.JSONObject;

public interface NetworkEventListener {


    void onSuccess(JSONObject object) throws JSONException;

    void onError(String string) ;
}
