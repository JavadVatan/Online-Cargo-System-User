package com.nobaar.android.dialog;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.global.Constants;


public class DiscountDialog extends AlertDialog implements View.OnClickListener {
    public static final int STYLE_NOTIFICATION = 0;
    public static final int STYLE_CONFIRMATION = 1;

    private Context mContext;
    private TextView tvTitle;
    private Button btnPositive, btnNegative;
    private EditText etInput;
    private DiscountDialogHandler mListener = null;
    private String mDefaultTitle, mDefaultPositive, mDefaultNegative;

    public DiscountDialog(Context context, DiscountDialogHandler mListener) {
        super(context);
        this.mContext = context;
        this.mListener = mListener;
        initDefaultValue();
    }

    private void initDefaultValue() {
        mDefaultTitle = mContext.getString(R.string.insert_discount_code);
        mDefaultPositive = mContext.getString(R.string.apply);
        mDefaultNegative = mContext.getString(R.string.cancel);
    }

    public DiscountDialog setTitleText(String title) {
        this.mDefaultTitle = title;
        return this;
    }

    public DiscountDialog setButtonText(String positive, String negative) {
        if (negative != null)
            this.mDefaultNegative = negative;

        if (positive != null)
            this.mDefaultPositive = positive;
        return this;
    }

    public DiscountDialog onCreateDialog() {
        initVariable();
        setDataVariable();
        show();
        return this;
    }

    private void initVariable() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.dialog_discount, null);
        btnPositive = dialogView.findViewById(R.id.dialog_btn_confirm);
        btnNegative = dialogView.findViewById(R.id.dialog_btn_negative);
        dialogView.findViewById(R.id.dialog_btn_more).setVisibility(View.GONE);
        etInput = dialogView.findViewById(R.id.dialog_et_input);
        tvTitle = dialogView.findViewById(R.id.dialog_tv_title);
        btnPositive.setTypeface(Constants.iranSenseLight);
        btnNegative.setTypeface(Constants.iranSenseLight);
        etInput.setTypeface(Constants.iranSenseLight);
        tvTitle.setTypeface(Constants.iranSenseBold);

        btnPositive.setVisibility(View.GONE);
        btnNegative.setVisibility(View.GONE);

        setView(dialogView);
    }

    private void setDataVariable() {
        tvTitle.setText(mDefaultTitle);
        btnNegative.setVisibility(View.GONE);
        btnPositive.setVisibility(View.VISIBLE);
        btnPositive.setText(mDefaultPositive);
        btnPositive.setOnClickListener(this);
        btnNegative.setVisibility(View.VISIBLE);
        btnNegative.setText(mDefaultNegative);
        btnNegative.setOnClickListener(this);
    }

    public DiscountDialog setCancelableDialog(boolean status, OnCancelListener listener) {
        setCancelable(status);
        if (listener != null)
            setOnCancelListener(listener);
        return this;
    }

    public DiscountDialog setCanceledOnTouchOutsideDialog(boolean status) {
        setCanceledOnTouchOutside(status);
        return this;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        destroyDialog();
    }

    private void destroyDialog() {
        dismiss();
        mListener.onCancelClicked();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_btn_negative:
                destroyDialog();
                break;

            case R.id.dialog_btn_confirm:
                manageConfirm();
                break;
        }
    }

    private void manageConfirm() {
        if (validation()) {
            dismiss();
            mListener.onConfirmClicked(etInput.getText().toString().trim());
        }
    }

    private boolean validation() {
        return etInput.getText().toString().trim().length() != 0;
    }

    public interface DiscountDialogHandler {
        void onConfirmClicked(String discountCode);

        void onCancelClicked();

    }

}

