package com.nobaar.android.dialog;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.global.Constants;


public class SimpleDialog extends AlertDialog implements View.OnClickListener {
    public static final int STYLE_NOTIFICATION = 0;
    public static final int STYLE_CONFIRMATION = 1;

    private Context mContext;
    private TextView tvTitle, tvMessage;
    private Button btnPositive, btnNegative, btnMore;
    private DialogConfirmationHandler interfaceEventListener = null;
    private int dialogStyle;
    private String mDefaultTitle, mDefaultPositive, mDefaultNegative, mDefaultMore, strMessage;

    public SimpleDialog(Context context, DialogConfirmationHandler listener, int dialogStyle) {
        super(context);
        this.mContext = context;
        this.interfaceEventListener = listener;
        this.dialogStyle = dialogStyle;
        initDefaultValue();
    }

    private void initDefaultValue() {
        mDefaultTitle = mContext.getString(R.string.alert);
        mDefaultPositive = mContext.getString(R.string.confirm);
        mDefaultNegative = mContext.getString(R.string.cancel);
        mDefaultMore = null;
    }

    public SimpleDialog setTitleText(String title) {
        this.mDefaultTitle = title;
        return this;
    }

    public SimpleDialog setButtonText(String positive, String negative) {
        if (negative != null)
            this.mDefaultNegative = negative;

        if (positive != null)
            this.mDefaultPositive = positive;
        return this;
    }

    public SimpleDialog setMessage(String message) {
        this.strMessage = message;
        return this;
    }

    public SimpleDialog setMoreButton(String strMore) {
        this.mDefaultMore = strMore;
        return this;
    }

    public SimpleDialog onCreateDialog() {
        initVariable();
        setDataVariable();
        show();
        return this;
    }

    private void initVariable() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.dialog_alert, null);
        btnPositive = dialogView.findViewById(R.id.dialog_btn_confirm);
        btnNegative = dialogView.findViewById(R.id.dialog_btn_negative);
        btnMore = dialogView.findViewById(R.id.dialog_btn_more);
        tvMessage = dialogView.findViewById(R.id.dialog_tv_message);
        tvTitle = dialogView.findViewById(R.id.dialog_tv_title);
        btnPositive.setTypeface(Constants.iranSenseLight);
        btnNegative.setTypeface(Constants.iranSenseLight);
        btnMore.setTypeface(Constants.iranSenseLight);
        tvMessage.setTypeface(Constants.iranSenseLight);
        tvTitle.setTypeface(Constants.iranSenseBold);

        btnPositive.setVisibility(View.GONE);
        btnNegative.setVisibility(View.GONE);
        btnMore.setVisibility(View.GONE);

        setView(dialogView);
    }

    private void setDataVariable() {

        tvTitle.setText(mDefaultTitle);
        tvMessage.setText(strMessage);

        switch (dialogStyle) {
            case STYLE_CONFIRMATION:
                manageStyleConfirmation();
                break;

            case STYLE_NOTIFICATION:
                manageStyleNotification();
                break;
        }

        if (mDefaultMore != null) {
            btnMore.setVisibility(View.VISIBLE);
            btnMore.setText(mDefaultMore);
            btnMore.setOnClickListener(this);
        }
    }

    private void manageStyleNotification() {
        btnNegative.setVisibility(View.GONE);
        btnPositive.setVisibility(View.VISIBLE);
        btnPositive.setText(mDefaultPositive);
        btnPositive.setOnClickListener(this);
    }

    private void manageStyleConfirmation() {
        manageStyleNotification();
        btnNegative.setVisibility(View.VISIBLE);
        btnNegative.setText(mDefaultNegative);
        btnNegative.setOnClickListener(this);
    }

    public SimpleDialog setCancelableDialog(boolean status, OnCancelListener listener) {
        setCancelable(status);
        if (listener != null)
            setOnCancelListener(listener);
        return this;
    }

    public SimpleDialog setCanceledOnTouchOutsideDialog(boolean status) {
        setCanceledOnTouchOutside(status);
        return this;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        destroyDialog();
    }

    private void destroyDialog() {
        dismiss();
        interfaceEventListener.onNegativeDialogConfirmationClicked();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_btn_negative:
                destroyDialog();
                break;

            case R.id.dialog_btn_confirm:
                dismiss();
                interfaceEventListener.onPositiveDialogConfirmationClicked();
                break;

            case R.id.dialog_btn_more:
                dismiss();
                interfaceEventListener.onMoreDialogConfirmationClicked();
                break;
        }
    }

    public static class DialogConfirmationHandler {
        public DialogConfirmationHandler() {
        }

        public void onPositiveDialogConfirmationClicked() {
        }

        public void onNegativeDialogConfirmationClicked() {
        }

        public void onMoreDialogConfirmationClicked() {
        }
    }

}

