package com.nobaar.android.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.struct.StructDriverInfo;
import com.squareup.picasso.Picasso;


public class DriverInfoDialog extends AlertDialog implements View.OnClickListener {
    private Context mContext;
    private StructDriverInfo structDriver;

    public DriverInfoDialog(Context context, StructDriverInfo structDriver) {
        super(context);
        this.mContext = context;
        this.structDriver = structDriver;
        initVariable(structDriver);
    }

    public DriverInfoDialog setCancelableDialog(boolean status, OnCancelListener listener) {
        setCancelable(status);
        if (listener != null)
            setOnCancelListener(listener);
        return this;
    }

    public DriverInfoDialog setCanceledOnTouchOutsideDialog(boolean status) {
        setCanceledOnTouchOutside(status);
        return this;
    }

    private void initVariable(StructDriverInfo structDriver) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.dialog_driver_info, null);
        GlobalFunction.getInstance().overrideFonts(mContext, dialogView);
        ImageView ivPhoto = dialogView.findViewById(R.id.dialog_driver_iv_photo);
        TextView tvName = dialogView.findViewById(R.id.dialog_driver_tv_name);
        TextView tvDetail = dialogView.findViewById(R.id.dialog_driver_tv_detail);
        TextView tvRate = dialogView.findViewById(R.id.dialog_driver_tv_rate);

        Picasso.with(mContext).load(structDriver.getPhotoUrl()).into(ivPhoto);
        tvName.setText(structDriver.getDriverName());
        tvRate.setText(structDriver.getRate());
        tvDetail.setText(structDriver.getCarName());
        tvName.setTypeface(Constants.iranSenseBold);

        dialogView.findViewById(R.id.dialog_driver_iv_call).setOnClickListener(this);
        dialogView.findViewById(R.id.dialog_driver_iv_message).setOnClickListener(this);

        setView(dialogView);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        destroyDialog();
    }

    private void destroyDialog() {
        dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_driver_iv_call:
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + structDriver.getPhoneNumber()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
                break;

            case R.id.dialog_driver_iv_message:
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"
                        + structDriver.getPhoneNumber())));
                break;
        }
    }

}

