package com.nobaar.android.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.nobaar.android.Application;
import com.nobaar.android.R;


/**
 * Created by MikroCom on 11/08/2017.
 */

public class CodePicker extends LinearLayout {

    final int DEFAULT_COUNT = 4;
    private CodePickerHandler mListener;
    private int count;

    public CodePicker(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(attrs);
    }

    public void setListener(CodePickerHandler mListener) {
        this.mListener = mListener;
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CodePicker);
            count = a.getInteger(R.styleable.CodePicker_count, DEFAULT_COUNT);

            setOrientation(HORIZONTAL);
            setGravity(Gravity.CENTER);

            for (int i = 0; i < count; i++) {
                addView(generateDigitContainer(i));
            }
            findViewById(0).setEnabled(true);
            a.recycle();
        }
    }

    private EditText generateDigitContainer(final int id) {
        final EditText et = new EditText(getContext());

        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 0, 10, 0);

        et.setId(id);
        et.setTextSize(30);
       // et.setMaxEms(1);
        et.setEnabled(false);
        et.setTextColor(getResources().getColor(R.color.login_text_color));
        et.setGravity(Gravity.CENTER);
        et.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        et.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});
        et.setLayoutParams(params);
        et.setBackground(Application.getInstance().getContext().getResources().getDrawable(R.drawable.bg_input_text));
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et.getText().length() > 0) {
                    if (findViewById(id + 1) != null) {
                        findViewById(id + 1).setEnabled(true);
                        findViewById(id + 1).requestFocus();
                        findViewById(id).setEnabled(false);
                    }
                    if (id == count - 1) {
                        if (mListener != null)
                            mListener.codePickerStatus(true);
                    }
                } else {
                    if (findViewById(id - 1) != null) {
                        findViewById(id - 1).requestFocus();
                        findViewById(id-1).setEnabled(true);
                        findViewById(id).setEnabled(false);


                    }
                    if (id != count - 1) {
                        if (mListener!=null)
                            mListener.codePickerStatus(false);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        return et;
    }

    public String getCode() {
        String code = "";
        for (int i = 0; i < count; i++) {
            code += ((EditText) findViewById(i)).getText().toString();
        }

        return code;
    }

    public interface CodePickerHandler {
        void codePickerStatus(boolean status);
    }
}
