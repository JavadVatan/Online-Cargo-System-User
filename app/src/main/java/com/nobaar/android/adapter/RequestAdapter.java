package com.nobaar.android.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.global.Constants;
import com.nobaar.android.struct.StructRequest;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Javad Vatan on 10/23/2017.
 */

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {
    private List<StructRequest> dataList;
    private RequestAdapterHandler mListener;
    private Context mContext;
    private CountDownTimer mCountDownTimer;

    public RequestAdapter(RequestAdapterHandler AdapterHandler,
                          List<StructRequest> dataList) {
        this.dataList = dataList;
        this.mListener = AdapterHandler;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_request, parent, false);
        mContext = parent.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        manageLines(holder, position);

        holder.ivStatus.setImageResource(dataList.get(position).getStatusId());
        holder.flStatus.setBackgroundResource(dataList.get(position).getBgStatusId());
        holder.ivShow.setImageResource(dataList.get(position).getShowId());
        holder.tvTitle.setText(dataList.get(position).getTitle());

        manageDetailText(holder, position);
        if (dataList.get(position).getShowId() == R.drawable.ic_refresh) {
            setAnimation(holder);
            dataList.get(position).setClickable(false);
        } else if (dataList.get(position).getShowId() == R.drawable.ic_show) {
            holder.ivShow.clearAnimation();
            dataList.get(position).setClickable(true);
        }
    }

    private void manageDetailText(final ViewHolder holder, int position) {
        if (dataList.get(position).getDetail() != null && !dataList.get(position).isTimer()) {
            holder.tvDetail.setVisibility(View.VISIBLE);
            holder.tvDetail.setText(dataList.get(position).getDetail());
            if (mCountDownTimer != null) mCountDownTimer.cancel();
        } else if (dataList.get(position).getDetail() != null && dataList.get(position).isTimer()) {
            manageTimer(holder, position);
        } else
            holder.tvDetail.setVisibility(View.GONE);

    }

    private void manageTimer(final ViewHolder holder, int position) {
        holder.tvDetail.setVisibility(View.VISIBLE);
        final long[] time = {Math.abs(Long.parseLong(dataList.get(position).getDetail()))};
        mCountDownTimer = new CountDownTimer(1000000000, 1000) {
            @SuppressLint({"WrongConstant", "SetTextI18n"})
            public void onTick(long millisUntilFinished) {
                @SuppressLint("DefaultLocale") String timeFormat = String.format("%02d:%02d:%02d",
                        TimeUnit.MILLISECONDS.toHours(time[0] * 1000),
                        TimeUnit.MILLISECONDS.toMinutes(time[0] * 1000) % 60,
                        TimeUnit.MILLISECONDS.toSeconds(time[0] * 1000) % 60);
                time[0]--;
                holder.tvDetail.setText(timeFormat);
            }

            public void onFinish() {
            }
        };
        mCountDownTimer.start();
    }

    private void setAnimation(final ViewHolder holder) {
        Animation rotation = AnimationUtils.loadAnimation(mContext, R.anim.rotate);
        holder.ivShow.setAnimation(rotation);
        rotation.start();
    }

    private void manageLines(final ViewHolder holder, int position) {
        if (position == 0)
            holder.vLineUp.setVisibility(View.INVISIBLE);
        else
            holder.vLineUp.setVisibility(View.VISIBLE);


        if (dataList.size() == 1 && position == 0)
            holder.vLineDown.setVisibility(View.INVISIBLE);
        else
            holder.vLineDown.setVisibility(View.VISIBLE);

        if (position == dataList.size() - 1)
            holder.vLineDown.setVisibility(View.INVISIBLE);
        else
            holder.vLineDown.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public interface RequestAdapterHandler {
        void onItemClicked(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle, tvDetail;
        private ImageView ivStatus, ivShow;
        private View vLineUp, vLineDown;
        private FrameLayout flStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.item_request_tv_title);
            tvDetail = itemView.findViewById(R.id.item_request_tv_detail);
            ivStatus = itemView.findViewById(R.id.item_request_iv_status);
            ivShow = itemView.findViewById(R.id.item_request_iv_show);
            flStatus = itemView.findViewById(R.id.item_request_fl_status);
            vLineDown = itemView.findViewById(R.id.item_request_v_line_down);
            vLineUp = itemView.findViewById(R.id.item_request_v_line_up);

            tvTitle.setTypeface(Constants.iranSenseLight);
            tvDetail.setTypeface(Constants.iranSenseLight);

            ivShow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (dataList.get(getAdapterPosition()).isClickable())
                        mListener.onItemClicked(getAdapterPosition());
                }
            });
        }
    }
}
