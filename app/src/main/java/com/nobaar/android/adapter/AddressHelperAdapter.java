package com.nobaar.android.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.db.StructAddressHelper;
import com.nobaar.android.global.Constants;

import java.util.List;

/**
 * Created by Javad Vatan on 10/15/2017.
 */

public class AddressHelperAdapter extends RecyclerView.Adapter<AddressHelperAdapter.ViewHolder> {
    private List<StructAddressHelper> dataList;
    private AddressHelperAdapterHandler mListener;
    private boolean isRemoveIconEnable=true;

    public AddressHelperAdapter(AddressHelperAdapterHandler AdapterHandler,
                                List<StructAddressHelper> dataList) {
        this.dataList = dataList;
        this.mListener = AdapterHandler;
    }

    public AddressHelperAdapter setDisableRemoveIcon(boolean status){
        isRemoveIconEnable=status;
        return this;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_address_helper, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.tvAddress.setText(dataList.get(position).getAddress());
        if (!isRemoveIconEnable)
            holder.ivRemove.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public interface AddressHelperAdapterHandler {
        void onItemAddressClicked(int position);

        void onItemRemoveBookmarkClicked(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvAddress;
        private ImageView ivRemove;
        private LinearLayout llParentContent;
        public ViewHolder(View itemView) {
            super(itemView);
            tvAddress = itemView.findViewById(R.id.item_bookmark_tv_title);
            ivRemove = itemView.findViewById(R.id.item_bookmark_iv_remove);
            llParentContent=itemView.findViewById(R.id.item_bookmark_ll_parent_content);
            tvAddress.setTypeface(Constants.iranSenseLight);

            llParentContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemAddressClicked(getAdapterPosition());
                }
            });
            ivRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemRemoveBookmarkClicked(getAdapterPosition());
                }
            });

        }
    }
}
