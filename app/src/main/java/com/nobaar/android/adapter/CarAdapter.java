package com.nobaar.android.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nobaar.android.R;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.struct.StructCar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public abstract class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {

    private RecyclerView mRecyclerView;
    private List<StructCar> dataList;
    private Context mContext;
    private int posCenter = Constants.NO_DATA;
    private int currentIndex;

    public CarAdapter(List<StructCar> dataList) {
        this.dataList = dataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_car, parent, false);
        mContext = parent.getContext();

        return new ViewHolder(v);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mRecyclerView = recyclerView;
    }

    @Override
    public void onViewAttachedToWindow(ViewHolder holder) {
        super.onViewAttachedToWindow(holder);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getVisibleItems();
            }
        }, 200);
    }

    private List<ViewHolder> getVisibleItems() {
        List<ViewHolder> holders = new ArrayList<>();
        int size = mRecyclerView.getLayoutManager().getChildCount();
        for (int i = 0; i < size; i++) {
            ViewHolder holder = (ViewHolder) mRecyclerView.getChildViewHolder
                    (mRecyclerView.getLayoutManager().getChildAt(i));
            holders.add(holder);
            // holders.get(i).vTvMonth.setTextColor(mContext.getResources().getColor(R.color.history_inactive_date));
        }
        posCenter = size - 2;
        currentCarSelected(holders.get(posCenter).getLayoutPosition());
        currentIndex = posCenter;

        return holders;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        ViewGroup.LayoutParams layoutParams = holder.itemView.getLayoutParams();
        if (dataList.get(position).getCarImage() == Constants.NO_DATA) {
            holder.ivCar.setImageResource(R.drawable.ic_neysan);
            holder.ivCar.setVisibility(View.INVISIBLE);
            layoutParams.width = GlobalFunction.getInstance().getWidthScreenSize(mContext) / 3;
            holder.itemView.setLayoutParams(layoutParams);
        } else {
            holder.ivCar.setVisibility(View.VISIBLE);
            layoutParams.width = (int) (GlobalFunction.getInstance().getWidthScreenSize(mContext) / 1.9);
            holder.itemView.setLayoutParams(layoutParams);
            manageCarImage(holder, position);
        }
    }

    private void manageCarImage(final ViewHolder holder, int position) {
        if (dataList.get(position).isEnable())
            Picasso.with(mContext)
                    .load(dataList.get(position).getPicEnable())
                    .into(holder.ivCar);
        else
            Picasso.with(mContext)
                    .load(dataList.get(position).getPicDisable())
                    .into(holder.ivCar);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public abstract void currentCarSelected(int index);

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivCar;

        public ViewHolder(View itemView) {
            super(itemView);
            ivCar = itemView.findViewById(R.id.item_car_iv_photo);

        }
    }
}

