package com.nobaar.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.global.Constants;
import com.nobaar.android.struct.StructTransportation;

import java.util.List;

/**
 * Created by Javad on 8/5/2016.
 */
public class TransportationAdapter extends RecyclerView.Adapter<TransportationAdapter.ViewHolder> {
    private List<StructTransportation> dataList;
    private Context mContext;

    public TransportationAdapter(List<StructTransportation> dataList) {
        this.dataList = dataList;
    }

    @Override
    public TransportationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_transportaion, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final TransportationAdapter.ViewHolder holder, int position) {
        holder.tvOrderName.setText(dataList.get(position).getOrderName());
        holder.tvOrderStatus.setText(dataList.get(position).getOrderStatus());
        holder.tvOrderDate.setText(dataList.get(position).getOrderDate());
        holder.ivCar.setImageResource(dataList.get(position).getImgCar());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvOrderName, tvOrderDate, tvOrderStatus;
        private ImageView ivCar;

        public ViewHolder(View itemView) {
            super(itemView);
            ivCar = itemView.findViewById(R.id.item_transportation_iv_car);
            tvOrderName = itemView.findViewById(R.id.item_transportation_tv_order_name);
            tvOrderDate = itemView.findViewById(R.id.item_transportation_tv_order_date);
            tvOrderStatus = itemView.findViewById(R.id.item_transportation_tv_order_status);

            tvOrderDate.setTypeface(Constants.iranSenseLight);
            tvOrderName.setTypeface(Constants.iranSenseBold);
            tvOrderStatus.setTypeface(Constants.iranSenseLight);
        }
    }
}
