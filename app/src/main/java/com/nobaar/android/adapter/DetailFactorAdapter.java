package com.nobaar.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.global.Constants;
import com.nobaar.android.struct.StructDetailFactor;

import java.util.List;

public class DetailFactorAdapter extends RecyclerView.Adapter<DetailFactorAdapter.ViewHolder> {

    private RecyclerView mRecyclerView;
    private List<StructDetailFactor> dataList;
    private Context mContext;

    public DetailFactorAdapter(List<StructDetailFactor> dataList) {
        this.dataList = dataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail_factor, parent, false);
        mContext = parent.getContext();

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.tvTitle.setText(dataList.get(position).getTitle());
        holder.tvValue.setText(dataList.get(position).getValue());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle, tvValue;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.item_detail_factor_tv_title);
            tvValue = itemView.findViewById(R.id.item_detail_factor_tv_value);
            tvTitle.setTypeface(Constants.iranSenseLight);
            tvValue.setTypeface(Constants.iranSenseLight);
        }
    }
}

