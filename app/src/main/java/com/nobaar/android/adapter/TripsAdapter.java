package com.nobaar.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.global.Constants;
import com.nobaar.android.struct.StructTrips;

import java.util.List;

/**
 * Created by Javad on 8/5/2016.
 */
public class TripsAdapter extends RecyclerView.Adapter<TripsAdapter.ViewHolder> {
    private List<StructTrips> dataList;
    private Context mContext;

    public TripsAdapter(List<StructTrips> dataList) {
        this.dataList = dataList;
    }

    @Override
    public TripsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trips, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final TripsAdapter.ViewHolder holder, int position) {
        holder.tvOrigin.setText(mContext.getString(R.string.trips_origin) + dataList.get(position).getOrigin());
        holder.tvBarcode.setText(mContext.getString(R.string.trips_barcode) + dataList.get(position).getBarcode());
        holder.tvDate.setText(mContext.getString(R.string.trips_date) + dataList.get(position).getDate());
        holder.tvDestination.setText(mContext.getString(R.string.trips_destination) + dataList.get(position).getDestination());
        holder.tvDriver.setText(dataList.get(position).getDriver() + mContext.getString(R.string.trips_driver));
        holder.tvPrice.setText(mContext.getString(R.string.trips_price) + dataList.get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvOrigin, tvDestination, tvDriver, tvPrice, tvDate, tvBarcode;

        public ViewHolder(View itemView) {
            super(itemView);
            tvOrigin = itemView.findViewById(R.id.item_trips_tv_origin);
            tvDestination = itemView.findViewById(R.id.item_trips_tv_destination);
            tvDriver = itemView.findViewById(R.id.item_trips_tv_driver);
            tvPrice = itemView.findViewById(R.id.item_trips_tv_price);
            tvDate = itemView.findViewById(R.id.item_trips_tv_calender);
            tvBarcode = itemView.findViewById(R.id.item_trips_tv_barcode);

            tvOrigin.setTypeface(Constants.iranSenseLight);
            tvDestination.setTypeface(Constants.iranSenseLight);
            tvDriver.setTypeface(Constants.iranSenseLight);
            tvPrice.setTypeface(Constants.iranSenseLight);
            tvDate.setTypeface(Constants.iranSenseLight);
            tvBarcode.setTypeface(Constants.iranSenseLight);
        }
    }
}
