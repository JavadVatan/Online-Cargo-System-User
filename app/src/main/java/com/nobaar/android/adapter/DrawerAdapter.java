package com.nobaar.android.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.global.Constants;
import com.nobaar.android.struct.StructDrawer;

import java.util.List;

/**
 * Created by Javad on 8/5/2016.
 */
public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ViewHolder> {
    private List<StructDrawer> dataList;
    private DrawerAdapterHandler mListener;

    public DrawerAdapter(DrawerAdapterHandler DrawerAdapterHandler,
                         List<StructDrawer> dataList) {
        this.dataList = dataList;
        this.mListener = DrawerAdapterHandler;
    }

    @Override
    public DrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_drawer, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final DrawerAdapter.ViewHolder holder, int position) {
       /* DbHandler dbHandler = new DbHandler(mContext);
        dbHandler.openDataBase();*/
        int id = dataList.get(position).getIcon();

        if (id == Constants.NO_DATA) {
            holder.itemView.setVisibility(View.INVISIBLE);
            return;
        }
        holder.imgIcon.setImageResource(id);

        holder.tvName.setText(dataList.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public interface DrawerAdapterHandler {
        void onItemDrawerClicked(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvDetail;
        private ImageView imgIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.itemDrawerText);
            imgIcon = (ImageView) itemView.findViewById(R.id.itemDrawerImage);
            tvName.setTypeface(Constants.iranSenseLight);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemDrawerClicked(getAdapterPosition());
                }
            });

        }
    }
}
