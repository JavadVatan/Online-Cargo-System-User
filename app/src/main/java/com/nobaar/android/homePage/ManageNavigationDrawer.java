package com.nobaar.android.homePage;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.Util;
import com.nobaar.android.activity.AboutActivity;
import com.nobaar.android.activity.DiscountActivity;
import com.nobaar.android.activity.HomeActivity;
import com.nobaar.android.activity.LoginActivity;
import com.nobaar.android.activity.ProfileActivity;
import com.nobaar.android.activity.RuleActivity;
import com.nobaar.android.activity.SupportActivity;
import com.nobaar.android.activity.TransportationActivity;
import com.nobaar.android.adapter.DrawerAdapter;
import com.nobaar.android.global.AppPreferences;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.struct.StructDrawer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.nobaar.android.global.Constants.NO_DATA;

/**
 * Created by Javad Vatan on 11/19/2017.
 */

public class ManageNavigationDrawer implements DrawerAdapter.DrawerAdapterHandler, LifecycleObserver {
    private Context mContext;
    private View currView;
    private DrawerLayout mDrawerLayout;
    private DrawerAdapter drawerAdapter;
    private RecyclerView rvDrawer;
    private TextView tvUserName, tvUserCredit, tvUserPhone;
    private CircleImageView civUser;
    private AppPreferences preferences;

    public ManageNavigationDrawer(Context mContext, LifecycleOwner lifecycleOwner, View currView) {
        lifecycleOwner.getLifecycle().addObserver(this);
        this.mContext = mContext;
        this.currView = currView;
        initVariable();
        initUserData();
        initDrawer();
    }

    private void initVariable() {
        preferences = AppPreferences.getInstance();
        tvUserName = currView.findViewById(R.id.activity_home_profile_tv_name);
        tvUserPhone = currView.findViewById(R.id.activity_home_profile_tv_user_phone);
        tvUserCredit = currView.findViewById(R.id.activity_home_profile_tv_credit);
        civUser = currView.findViewById(R.id.activity_home_profile_image);
        mDrawerLayout = currView.findViewById(R.id.drawer_layout);
        rvDrawer = currView.findViewById(R.id.mainDrawerList);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private void initUserData() {
        tvUserName.setText(preferences.getUserName());
        tvUserPhone.setText(preferences.getUserPhoneNumber());
        tvUserCredit.setText(String.format(" %s ریال ", Util.getDecimalFormattedString(preferences.getUserCredit())));

        if (!preferences.getUserPhotoUrl().equals("")) {
            Picasso.with(mContext)
                    .load(preferences.getUserPhotoUrl())
                    .placeholder(R.drawable.ic_avatar)
                    .into(civUser);
        }
    }

    private void initDrawer() {
        LinearLayout llRootDrawer = currView.findViewById(R.id.navigation_drawer_ll_root);
        ViewGroup.LayoutParams params = llRootDrawer.getLayoutParams();
        int width = (int) (GlobalFunction.getInstance().getWidthScreenSize(mContext) / 1.3);
        params.width = width;
        llRootDrawer.setLayoutParams(params);

        List<StructDrawer> dataList = new ArrayList<StructDrawer>();
        int[] allIconDrawer = {R.drawable.is_drawer_user,
                R.drawable.ic_drawer_transportion, R.drawable.ic_drawer_free_trip,
                R.drawable.ic_drawer_support, R.drawable.ic_drawer_rules,
                R.drawable.ic_drawer_about, NO_DATA,
                R.drawable.ic_drawer_exit};

        String[] allNameDrawer = mContext.getResources().getStringArray(R.array.drawer_items);

        for (int i = 0; i < allIconDrawer.length; i++) {
            dataList.add(new StructDrawer(allNameDrawer[i], allIconDrawer[i]));
        }

        drawerAdapter = new DrawerAdapter(this, dataList);
        rvDrawer.setHasFixedSize(true);
        rvDrawer.setLayoutManager(new LinearLayoutManager(mContext));
        rvDrawer.setAdapter(drawerAdapter);
    }

    @Override
    public void onItemDrawerClicked(int position) {
        switch (position) {
            case 0:
                mContext.startActivity(new Intent(mContext, ProfileActivity.class));
                break;

           /* case 1:
                mContext.startActivity(new Intent(mContext, MessageActivity.class));
                break;
*/
            case 1:
                mContext.startActivity(new Intent(mContext, TransportationActivity.class));
                break;

            case 2:
                mContext.startActivity(new Intent(mContext, DiscountActivity.class));
                break;

            case 3:
                mContext.startActivity(new Intent(mContext, SupportActivity.class));
                break;

            case 4:
                mContext.startActivity(new Intent(mContext, RuleActivity.class));

                break;

            case 5:
                mContext.startActivity(new Intent(mContext, AboutActivity.class));
                break;

            case 7:
                AppPreferences.getInstance().clearPreferences();
                mContext.startActivity(new Intent(mContext, LoginActivity.class));
                ((HomeActivity) mContext).finish();
                break;

        }
        mDrawerLayout.closeDrawers();
    }

    public DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }
}
