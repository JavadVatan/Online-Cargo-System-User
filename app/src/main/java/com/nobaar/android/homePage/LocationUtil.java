package com.nobaar.android.homePage;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.location.Location;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.nobaar.android.R;
import com.nobaar.android.dialog.SimpleDialog;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;

/**
 * Created by Javad Vatan on 11/19/2017.
 */

public class LocationUtil implements LifecycleObserver {
    private Context mContext;
    private SimpleDialog dlgGps;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private LocationUtilHandler mListener;

    public LocationUtil(Context mContext, LifecycleOwner lifecycleOwner, LocationUtilHandler mListener) {
        this.mContext = mContext;
        this.mListener = mListener;
        checkGps();
        initLocationVariable();
        createLocationRequest();
        lifecycleOwner.getLifecycle().addObserver(this);
    }

    private boolean checkGps() {
        if (!GlobalFunction.getInstance().isGpsOn(mContext)) {
            showDialogGps();
            return false;
        }
        return true;
    }

    private void initLocationVariable() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext);
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(Constants.UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(Constants.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if (locationResult.getLastLocation() != null) {
                    mListener.updateUi(locationResult.getLastLocation());
                    removeLocationUpdates();
                }
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);
                if (!locationAvailability.isLocationAvailable()) {
                    updateUiFromLastLocation();
                }
            }
        };

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback, Looper.myLooper());
    }

    private void updateUiFromLastLocation() {
        Task<Location> locationResult = getLastLocation();
        if (locationResult != null) {
            locationResult.addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        Location location = task.getResult();
                        if (location != null) {
                            mListener.updateUi(location);
                        }
                    }
                }
            });
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void removeLocationUpdates() {
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        } catch (SecurityException unlikely) {
            unlikely.printStackTrace();
        }
    }

    private void showDialogGps() {
        if (dlgGps == null || !dlgGps.isShowing()) {
            SimpleDialog.DialogConfirmationHandler handler = new SimpleDialog.
                    DialogConfirmationHandler() {

                @Override
                public void onPositiveDialogConfirmationClicked() {
                    GlobalFunction.getInstance().openGPSSetting(mContext);
                }
            };

            dlgGps = new SimpleDialog(mContext, handler,
                    SimpleDialog.STYLE_CONFIRMATION)
                    .setMessage(mContext.getString(R.string.error_gps_disable))
                    .setButtonText(mContext.getString(R.string.check_gps), null)
                    .setCancelableDialog(false, null)
                    .setCanceledOnTouchOutsideDialog(true)
                    .onCreateDialog();
        }
    }

    public Task<Location> getLastLocation() {
        if (checkGps())
            return mFusedLocationClient.getLastLocation();

        return null;

    }

    public interface LocationUtilHandler {
        void updateUi(Location location);
    }


}
