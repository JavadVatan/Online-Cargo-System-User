package com.nobaar.android.homePage;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.nobaar.android.Application;
import com.nobaar.android.activity.NoInternetConnectionActivity;

import java.io.IOException;
import java.util.List;

/**
 * Created by Javad Vatan on 11/19/2017.
 */

public class GetAddress extends AsyncTask<LatLng, Void, String> {
    private GetAddressHandler mListener;
    private Context mContext;
    private Geocoder mGeocoder;


    public GetAddress(Context mContext, Geocoder mGeocoder, GetAddressHandler mListener) {
        this.mListener = mListener;
        this.mContext = mContext;
        this.mGeocoder = mGeocoder;
    }

    @Override
    protected void onPreExecute() {
        if (!Application.getInstance().isConnectionAnyInternet()) {
            mContext.startActivity(new Intent(mContext, NoInternetConnectionActivity.class));
            return;
        }
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(LatLng... latLngs) {
        String resLocationName = "";
        LatLng latLng = latLngs[0];
        List<Address> matches = null;
        try {
            matches = mGeocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (matches != null) {
            Address bestMatch = (matches.isEmpty() ? null : matches.get(0));
            if (bestMatch != null) {
                String city = bestMatch.getLocality();
                String featureName = bestMatch.getFeatureName();
                String address = bestMatch.getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                resLocationName = city + " - " + featureName;


            }
            if (resLocationName.length() == 0)
                resLocationName = "";
        }
        return resLocationName;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        mListener.onTaskCompleted(s);
    }

    public interface GetAddressHandler {
        void onTaskCompleted(String response);
    }
}