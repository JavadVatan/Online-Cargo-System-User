package com.nobaar.android.struct;

/**
 * Created by Javad Vatan on 9/8/2017.
 */

public class StructTransportation {
    private String orderName, orderDate, orderStatus;
    private int imgCar;

    public StructTransportation() {
    }

    public StructTransportation(String orderName, String orderDate, String orderStatus, int imgCar) {
        this.orderName = orderName;
        this.orderDate = orderDate;
        this.orderStatus = orderStatus;
        this.imgCar = imgCar;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getImgCar() {
        return imgCar;
    }

    public void setImgCar(int imgCar) {
        this.imgCar = imgCar;
    }
}
