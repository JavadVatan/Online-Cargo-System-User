package com.nobaar.android.struct;

/**
 * Created by Javad Vatan on 10/17/2017.
 */

public class StructCar {
    private String title, picEnable, picDisable;
    private int id, carImage, status;
    private long date, price;
    private boolean isEnable;

    public StructCar(String title, int id, int carImage, int status, long date, long price,
                     String picEnable, String picDisable) {
        this.title = title;
        this.id = id;
        this.carImage = carImage;
        this.status = status;
        this.date = date;
        this.price = price;
        this.picEnable = picEnable;
        this.picDisable = picDisable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCarImage() {
        return carImage;
    }

    public void setCarImage(int carImage) {
        this.carImage = carImage;
    }

    public String getPicEnable() {
        return picEnable;
    }

    public void setPicEnable(String picEnable) {
        this.picEnable = picEnable;
    }

    public String getPicDisable() {
        return picDisable;
    }

    public void setPicDisable(String picDisable) {
        this.picDisable = picDisable;
    }

    public boolean isEnable() {
        return isEnable;
    }

    public void setEnable(boolean enable) {
        isEnable = enable;
    }
}
