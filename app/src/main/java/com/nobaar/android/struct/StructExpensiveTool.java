package com.nobaar.android.struct;

/**
 * Created by Javad Vatan on 10/17/2017.
 */

public class StructExpensiveTool {
    private String title;
    private int id, status;
    private long date, price;
    private int counter;

    public StructExpensiveTool(String title, int id, int status, long date, long price) {
        this.title = title;
        this.id = id;
        this.status = status;
        this.date = date;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
