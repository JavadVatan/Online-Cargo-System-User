package com.nobaar.android.struct;

/**
 * Created by Javad Vatan on 10/19/2017.
 */

public class StructDetailFactor {
    private String title,value;

    public StructDetailFactor() {
    }

    public StructDetailFactor(String title, String value) {
        this.title = title;
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
