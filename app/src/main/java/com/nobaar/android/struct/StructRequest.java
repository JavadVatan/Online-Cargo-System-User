package com.nobaar.android.struct;

/**
 * Created by Javad Vatan on 11/20/2017.
 */

public class StructRequest {
    private String title, detail;
    private int imgId, bgId, showId;
    private boolean isClickable;
    private boolean isTimer;

    public StructRequest(String title, String detail, int imgId, int bgId, int showId) {
        this.title = title;
        this.detail = detail;
        this.imgId = imgId;
        this.bgId = bgId;
        this.showId = showId;
    }

    public StructRequest(String title, String detail, int imgId, int bgId, int showId, boolean isTimer) {
        this.title = title;
        this.detail = detail;
        this.imgId = imgId;
        this.bgId = bgId;
        this.showId = showId;
        this.isTimer = isTimer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getStatusId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    public int getBgStatusId() {
        return bgId;
    }

    public void setBgId(int bgId) {
        this.bgId = bgId;
    }

    public int getShowId() {
        return showId;
    }

    public void setShowId(int showId) {
        this.showId = showId;
    }

    public boolean isClickable() {
        return isClickable;
    }

    public void setClickable(boolean clickable) {
        isClickable = clickable;
    }

    public boolean isTimer() {
        return isTimer;
    }

    public void setTimer(boolean timer) {
        isTimer = timer;
    }
}
