package com.nobaar.android.struct;

/**
 * Created by Javad Vatan on 10/15/2017.
 */

public class StructBookmark {
   private String addressName;

    public StructBookmark(String addressName) {
        this.addressName = addressName;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }
}
