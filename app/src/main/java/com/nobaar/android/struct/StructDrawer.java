package com.nobaar.android.struct;

/**
 * Created by Javad on 8/5/2016.
 */
public class StructDrawer {
    int icon;
    String name;

    public StructDrawer(String name , int icon){
        this.name=name;
        this.icon=icon;
    }

    public int getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }
}
