package com.nobaar.android.struct;

public class StructDate {
    private int year;
    private int month;
    private int day;
    private int hourInDay;
    private int minute;

    public StructDate(int year, int month, int day, int hourInDay, int minute) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hourInDay = hourInDay;
        this.minute = minute;

    }

    public StructDate() {
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHourInDay() {
        return hourInDay;
    }

    public void setHourInDay(int hourInDay) {
        this.hourInDay = hourInDay;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }
}
