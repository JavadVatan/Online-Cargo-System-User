package com.nobaar.android.struct;

/**
 * Created by Javad Vatan on 9/8/2017.
 */

public class StructTrips {
    private String origin,destination,driver,price,date,barcode;

    public StructTrips() {
    }

    public StructTrips(String origin, String destination, String driver, String price, String date, String barcode) {
        this.origin = origin;
        this.destination = destination;
        this.driver = driver;
        this.price = price;
        this.date = date;
        this.barcode = barcode;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
