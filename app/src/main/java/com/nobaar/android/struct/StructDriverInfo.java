package com.nobaar.android.struct;

/**
 * Created by Javad Vatan on 11/21/2017.
 */

public class StructDriverInfo {
    private String driverName;
    private String carName;
    private String rate;
    private String phoneNumber;
    private String urlDriver;

    public StructDriverInfo(String driverName, String carName, String rate, String phoneNumber, String urlDriver) {
        this.driverName = driverName;
        this.carName = carName;
        this.rate = rate;
        this.phoneNumber = phoneNumber;
        this.urlDriver = urlDriver;
    }

    public String getDriverName() {
        return driverName;
    }

    public String getCarName() {
        return carName;
    }

    public String getRate() {
        return rate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPhotoUrl() {
        return urlDriver;
    }
}


