package com.nobaar.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.nobaar.android.db.SQLiteHelper;


public class Application extends MultiDexApplication {

    private static volatile Application instance;
    public SQLiteHelper mDbHelper;
    private volatile Context mContext;

    public static synchronized Application getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //  Stetho.initializeWithDefaults(this);

        instance = this;
        mContext = getApplicationContext();
        mDbHelper = new SQLiteHelper();


        MultiDex.install(this);
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

/*
    public boolean isFirstRun() {
        return getSharedPreferences().getBoolean(Constants.SHARED_PREF_FIRST_RUN, true);
    }

    public void setFirstRun(boolean isFirstRun) {
        getSharedPreferences().edit().putBoolean(Constants.SHARED_PREF_FIRST_RUN, isFirstRun).apply();
    }
*/

    public boolean isConnectionAnyInternet() {
        ConnectivityManager conMgr = (ConnectivityManager) mContext.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfoWiFi, networkInfoMobile;
        networkInfoMobile = conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        networkInfoWiFi = conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return (networkInfoWiFi != null &&
                networkInfoWiFi.getState() == NetworkInfo.State.CONNECTED) ||
                (networkInfoMobile != null &&
                        networkInfoMobile.getState() == NetworkInfo.State.CONNECTED);
    }

    public void collapseKeyboard(android.app.Activity activity) {
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void expandKeyboard(View view) {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

}
