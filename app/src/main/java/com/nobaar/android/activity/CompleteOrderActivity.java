package com.nobaar.android.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.time.RadialPickerLayout;
import com.mohamadamin.persianmaterialdatetimepicker.time.TimePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;
import com.nobaar.android.R;
import com.nobaar.android.Util;
import com.nobaar.android.adapter.CarAdapter;
import com.nobaar.android.adapter.ExpensiveToolAdapter;
import com.nobaar.android.global.AppPreferences;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.global.TemporaryPreferences;
import com.nobaar.android.network.NetworkEventListener;
import com.nobaar.android.network.OkHttpHandler;
import com.nobaar.android.struct.StructCar;
import com.nobaar.android.struct.StructDate;
import com.nobaar.android.struct.StructExpensiveTool;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import okhttp3.FormBody;


public class CompleteOrderActivity extends AppCompatActivity implements View.OnClickListener,
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {
    private RecyclerView rvCar, rvExpensiveTool;
    private TextView tvCurrCar, tvCarryWorkerCounter, tvCatWorkerCounter, tvDate, tvConfirm;
    private PersianCalendar mPersianCalendar;
    private SwitchCompat scCostThings;
    private List<StructCar> dataListCar = new ArrayList<>();
    private List<StructExpensiveTool> dataListExpensiveTool = new ArrayList<>();
    private ExpensiveToolAdapter adapterExpensiveTool;
    private CarAdapter adapterCar;
    private int currIndexCarSelected = Constants.NO_DATA;
    private StructDate userDate;
    private GlobalFunction function;
    private String strUserDate;
    private TemporaryPreferences prefTemp;
    private ProgressBar pbConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_order);
        initializer();
    }

    private void initializer() {
        callApiGetCar();
        callApiGetExpensiveTool();
        initVariable();
        manageExpensiveList();
        initListCar();
        initListExpensiveTool();
        setOnClick();
    }

    private void initVariable() {
        function = GlobalFunction.getInstance();
        function.overrideFonts(this, getWindow().getDecorView().getRootView());
        mPersianCalendar = new PersianCalendar();
        mPersianCalendar.setTimeZone(TimeZone.getTimeZone("GMT+03:30"));
        prefTemp = TemporaryPreferences.getInstance();

        rvCar = findViewById(R.id.activity_complete_order_rv_car);
        rvExpensiveTool = findViewById(R.id.activity_complete_order_rv_expensive_tool);
        tvCurrCar = findViewById(R.id.activity_complete_order_tv_car_name);
        tvCarryWorkerCounter = findViewById(R.id.activity_complete_order_tv_carry_worker_counter);
        tvCatWorkerCounter = findViewById(R.id.activity_complete_order_tv_cat_worker_counter);
        tvDate = findViewById(R.id.activity_complete_order_tv_show_date);
        tvConfirm = findViewById(R.id.activity_complete_tv_confirm);
        pbConfirm = findViewById(R.id.activity_complete_pb_confirm);
        scCostThings = findViewById(R.id.activity_complete_order_sc_cost_things);
        tvDate.setText(getString(R.string.please_choose_data_and_time));
        tvDate.setTypeface(Constants.iranSenseBold);
    }

    private void initListCar() {
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(rvCar);
        rvCar.setItemAnimator(new DefaultItemAnimator());
        rvCar.setNestedScrollingEnabled(false);
        rvCar.setHasFixedSize(true);
        rvCar.setLayoutManager(new LinearLayoutManager
                (this, LinearLayoutManager.HORIZONTAL, true));
        adapterCar = new CarAdapter(dataListCar) {
            @Override
            public void currentCarSelected(int index) {
                tvCurrCar.setText(dataListCar.get(index).getTitle());
                currIndexCarSelected = index;
                manageEnableCar(currIndexCarSelected);
            }
        };
        rvCar.setAdapter(adapterCar);
    }

    private void manageEnableCar(int index) {
        int lastEnableIndex = 0;
        if (dataListCar.get(index).isEnable())
            return;
        for (int i = 0; i < dataListCar.size(); i++) {
            if (dataListCar.get(i).isEnable()) {
                dataListCar.get(i).setEnable(false);
                lastEnableIndex = i;
            }
        }
        dataListCar.get(index).setEnable(true);

        adapterCar.notifyItemChanged(lastEnableIndex);
        adapterCar.notifyItemChanged(index);
    }

    private void initListExpensiveTool() {
        rvExpensiveTool.setNestedScrollingEnabled(false);
        rvExpensiveTool.setHasFixedSize(true);
        rvExpensiveTool.setLayoutManager(new LinearLayoutManager
                (this, LinearLayoutManager.VERTICAL, true));
        adapterExpensiveTool = new ExpensiveToolAdapter(dataListExpensiveTool);
        rvExpensiveTool.setAdapter(adapterExpensiveTool);
    }

    private void manageExpensiveList() {
        scCostThings.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                rvExpensiveTool.setVisibility(b ? View.VISIBLE : View.GONE);
                rvExpensiveTool.requestFocus(View.FOCUS_DOWN);
            }
        });
    }

    private void callApiGetCar() {

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    JSONArray objectArray = object.getJSONObject(Constants.JSON_DATA)
                            .getJSONArray(Constants.JSON_DATA);
                    manageSuccessAdiGetCar(objectArray);
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            private void manageSuccessAdiGetCar(JSONArray objectArray) throws JSONException {
                for (int i = 0; i < objectArray.length(); i++) {
                    JSONObject jsonObject = objectArray.getJSONObject(i);
                    dataListCar.add(new StructCar(
                            jsonObject.getString(Constants.JSON_TITLE),
                            jsonObject.getInt(Constants.JSON_ID),
                            R.drawable.ic_neysan,
                            jsonObject.getInt(Constants.JSON_STATUS),
                            jsonObject.getLong(Constants.JSON_DATE),
                            jsonObject.getLong(Constants.JSON_PRICE),
                            jsonObject.getString(Constants.JSON_PICTURE_ENABLE),
                            jsonObject.getString(Constants.JSON_PICTURE_DISABLE)));

                }
                if (dataListCar.size() != 0) {
                    dataListCar.add(0, new StructCar("", -1, -1, -1, -1, -1, "", ""));
                    dataListCar.add(new StructCar("", -1, -1, -1, -1, -1, "", ""));
                    dataListCar.get(2).setEnable(true);
                    adapterCar.notifyDataSetChanged();
                    findViewById(R.id.activity_complete_order_pb_car).setVisibility(View.INVISIBLE);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            rvCar.smoothScrollToPosition(2);
                        }
                    }, 200);
                }
            }

            @Override
            public void onError(String string) {
                GlobalFunction.getInstance().toast(CompleteOrderActivity.this, getString
                        (R.string.unexpected_error_in_get_card));
            }
        };

        new OkHttpHandler(this, Constants.API_GET_CAR, eventListener)
                .send();
    }

    private void callApiGetExpensiveTool() {
        NetworkEventListener eventListener = new NetworkEventListener() {

            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    JSONArray objectArray = object.getJSONArray(Constants.JSON_DATA);
                    for (int i = 0; i < objectArray.length(); i++) {
                        JSONObject jsonObject = objectArray.getJSONObject(i);
                        dataListExpensiveTool.add(new StructExpensiveTool(
                                jsonObject.getString(Constants.JSON_TITLE),
                                jsonObject.getInt(Constants.JSON_ID),
                                jsonObject.getInt(Constants.JSON_STATUS),
                                jsonObject.getLong(Constants.JSON_DATE),
                                jsonObject.getLong(Constants.JSON_PRICE)));
                    }

                    if (dataListExpensiveTool.size() != 0) {
                        adapterExpensiveTool.notifyDataSetChanged();
                    }

                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            @Override
            public void onError(String string) {
                GlobalFunction.getInstance().toast(CompleteOrderActivity.this, getString
                        (R.string.unexpected_error_in_get_expensive_tool));
            }
        };
        new OkHttpHandler(this, Constants.API_GET_EXPENSIVE_TOOL, eventListener)
                .send();
    }

    private void setOnClick() {
        ((TextView) findViewById(R.id.include_app_bar_tv_page_name)).setText(R.string.complete_order);
        findViewById(R.id.include_app_bar_iv_back).setVisibility(View.VISIBLE);
        int[] arraySetClick = new int[]{
                R.id.include_app_bar_iv_back, R.id.activity_complete_order_iv_carry_worker_plus,
                R.id.activity_complete_order_iv_carry_worker_mines, R.id.activity_complete_order_iv_cat_worker_mines,
                R.id.activity_complete_order_iv_cat_worker_plus, R.id.activity_complete_order_rl_select_date
                , R.id.activity_complete_order_rl_cost_things, R.id.activity_complete_tv_confirm};
        for (int i = 0; i < arraySetClick.length; i++) {
            findViewById(arraySetClick[i]).setOnClickListener(this);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.include_app_bar_iv_back:
                onBackPressed();
                break;
            case R.id.activity_complete_tv_confirm:
                manageConfirmPage();
                break;
            case R.id.activity_complete_order_rl_select_date:
                showDialogDate();
                break;
            case R.id.activity_complete_order_rl_cost_things:
                scCostThings.setChecked(!scCostThings.isChecked());
                break;
            case R.id.activity_complete_order_iv_cat_worker_plus:
                manageCounter(tvCatWorkerCounter, true);
                break;
            case R.id.activity_complete_order_iv_carry_worker_plus:
                manageCounter(tvCarryWorkerCounter, true);
                break;
            case R.id.activity_complete_order_iv_cat_worker_mines:
                manageCounter(tvCatWorkerCounter, false);
                break;
            case R.id.activity_complete_order_iv_carry_worker_mines:
                manageCounter(tvCarryWorkerCounter, false);
                break;

        }
    }

    private void manageConfirmPage() {
        if (validation()) {
            if (storeData())
                callApiSendOrder();
        }
    }

    private void callApiSendOrder() {
        tvConfirm.setVisibility(View.INVISIBLE);
        pbConfirm.setVisibility(View.VISIBLE);

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    tvConfirm.setVisibility(View.VISIBLE);
                    pbConfirm.setVisibility(View.INVISIBLE);

                    JSONArray objectArray = object.getJSONArray(Constants.JSON_DATA);
                    for (int i = 0; i < objectArray.length(); i++) {
                        JSONObject jsonObject = objectArray.getJSONObject(i);
                        String price = jsonObject.getString(Constants.JSON_PRICE);
                        storeOrderId(jsonObject.getString(Constants.JSON_ID), price);
                    }
                    startActivity(new Intent(CompleteOrderActivity.this,
                            FactorActivity.class));

                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                tvConfirm.setVisibility(View.VISIBLE);
                pbConfirm.setVisibility(View.INVISIBLE);
                GlobalFunction.getInstance().toast(CompleteOrderActivity.this, string);
            }
        };
        new OkHttpHandler(this, Constants.API_SEND_ORDER, eventListener)
                .addParam(addPostParameter()).send();
    }

    private FormBody.Builder addPostParameter() {
        FormBody.Builder formBuilder = new FormBody.Builder();

        formBuilder.add(Constants.JSON_USER_ID_LOWER_CASE, AppPreferences.getInstance().getUserId());
        formBuilder.add(Constants.JSON_VASAYEL_HAZINEDAR, prefTemp.getPrefString(TemporaryPreferences
                .PREF_VASAYEL_HAZINEHDAR, ""));
        formBuilder.add(Constants.JSON_START_ADDRESS_LAT, prefTemp.getOriginLat() + "");
        formBuilder.add(Constants.JSON_START_ADDRESS_LONG, prefTemp.getOriginLong() + "");
        formBuilder.add(Constants.JSON_START_ADDRESS, prefTemp.getPrefString
                (TemporaryPreferences.PREF_ADDRESS_ORIGIN, ""));
        formBuilder.add(Constants.JSON_END_ADDRESS_LAT, prefTemp.getDestinationLat() + "");
        formBuilder.add(Constants.JSON_END_ADDRESS_LONG, prefTemp.getDestinationLong() + "");
        formBuilder.add(Constants.JSON_END_ADDRESS, prefTemp.getPrefString(
                TemporaryPreferences.PREF_ADDRESS_DESTINATION, ""));
        formBuilder.add(Constants.JSON_PAY_WITH_RECIVIER, String.valueOf(prefTemp
                .getPrefBool(TemporaryPreferences.PREF_PAY_WITH_RECEIVER, false) ? 1 : 0));
        formBuilder.add(Constants.JSON_RECIVIER_NAME, prefTemp.getPrefString(
                TemporaryPreferences.PREF_RECEIVER_NAME, ""));
        formBuilder.add(Constants.JSON_RECIVIER_MOBILE, prefTemp.getPrefString(
                TemporaryPreferences.PREF_RECEIVER_MOBILE, ""));
        formBuilder.add(Constants.JSON_TIME, prefTemp.getPrefString(TemporaryPreferences
                .PREF_TIME_MILLISECOND, "-1"));
        formBuilder.add(Constants.JSON_BARBAR_WORKER, prefTemp.getPrefString(
                TemporaryPreferences.PREF_KARGAR_BAR, "0"));
        formBuilder.add(Constants.JSON_CHIDEMAN_WORKER, prefTemp.getPrefString(
                TemporaryPreferences.PREF_KARGAR_CHIDEMAN, "0"));
        formBuilder.add(Constants.JSON_ORIGIN_FLOOR, prefTemp.getPrefString(
                TemporaryPreferences.PREF_ORIGIN_FLOOR, "0"));
        formBuilder.add(Constants.JSON_ORIGIN_WALKING, prefTemp.getPrefString(
                TemporaryPreferences.PREF_ORIGIN_WALKING, "0"));
        formBuilder.add(Constants.JSON_DESTINATION_FLOOR, prefTemp.getPrefString(
                TemporaryPreferences.PREF_DESTINATION_FLOOR, "0"));
        formBuilder.add(Constants.JSON_DESTINATION_WALKING, prefTemp.getPrefString(
                TemporaryPreferences.PREF_DESTINATION_WALKING, "0"));
        formBuilder.add(Constants.JSON_CAR_ID, prefTemp.getPrefString(TemporaryPreferences
                .PREF_CAR_ID, "0"));

        return formBuilder;
    }

    private void storeOrderId(String orderId, String orderPrice) {
        TemporaryPreferences temporaryPreferences = TemporaryPreferences.getInstance();
        temporaryPreferences.addPrefString(TemporaryPreferences.PREF_ORDER_ID, orderId);
        temporaryPreferences.addPrefString(TemporaryPreferences.PREF_ORDER_PRICE, orderPrice);
    }

    private void showDialogDate() {
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                mPersianCalendar.getPersianYear(),
                mPersianCalendar.getPersianMonth(),
                mPersianCalendar.getPersianDay()
        );
        dpd.setThemeDark(false);
        dpd.show(getFragmentManager(), getString(R.string.Selectt_date));
    }

    private void showDialogTime() {
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                this,
                mPersianCalendar.get(PersianCalendar.HOUR_OF_DAY),
                mPersianCalendar.get(PersianCalendar.MINUTE),
                true
        );
        tpd.setThemeDark(false);
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

            }
        });
        tpd.show(getFragmentManager(), getString(R.string.select_time));
    }

    private void manageCounter(TextView tvCounter, boolean isPlus) {
        int counter;

        counter = Integer.parseInt(tvCounter.getText().toString());

        if (isPlus)
            counter++;
        else if (counter > 0)
            counter--;

        tvCounter.setText(String.valueOf(counter));
    }


    private boolean validation() {
        if (userDate == null) {
            function.toast(this, getString(R.string.please_choose_data_and_time));
            return false;
        }
        if (currIndexCarSelected == Constants.NO_DATA) {
            function.toast(this, getString(R.string.please_choose_car));
            return false;
        }
        return true;
    }

    private boolean storeData() {
        TemporaryPreferences pref = TemporaryPreferences.getInstance();
        pref.addPrefString(TemporaryPreferences.PREF_CAR_ID, dataListCar.get(currIndexCarSelected)
                .getId() + "");
        pref.addPrefString(TemporaryPreferences.PREF_KARGAR_BAR, tvCarryWorkerCounter
                .getText().toString());
        pref.addPrefString(TemporaryPreferences.PREF_KARGAR_CHIDEMAN, tvCatWorkerCounter
                .getText().toString());

        Util util = new Util();
        long timeInMilliSecond = util.convertDateAndTimeToMilliSec(this, userDate);
        if (timeInMilliSecond == Constants.NO_DATA) {
            function.toast(this, getString(R.string.error_in_parsing_date));
            return false;
        }
        pref.addPrefString(TemporaryPreferences.PREF_TIME_MILLISECOND, String.valueOf(timeInMilliSecond));
        pref.addPrefString(TemporaryPreferences.PREF_TIME_STRING, strUserDate);
        try {
            pref.addPrefString(TemporaryPreferences.PREF_VASAYEL_HAZINEHDAR, makeJsonExpensiveTool());
        } catch (JSONException e) {
            e.printStackTrace();
            function.toast(this, getString(R.string.error_make_expensive_tool_json));
            return false;
        }

        return true;
    }

    private String makeJsonExpensiveTool() throws JSONException {
        if (rvExpensiveTool.getVisibility() == View.VISIBLE) {
            JSONObject mainObject = new JSONObject();
            JSONArray mainArray = new JSONArray();
            for (int i = 0; i < dataListExpensiveTool.size(); i++) {
                JSONObject object = new JSONObject();
                object.put(Constants.JSON_ID, dataListExpensiveTool.get(i).getId());
                object.put(Constants.JSON_COUNT, dataListExpensiveTool.get(i).getCounter());
                object.put(Constants.JSON_SUBJECT, dataListExpensiveTool.get(i).getTitle());
                mainArray.put(object);
            }
            mainObject.put(Constants.JSON_DATA, mainArray);
            return mainObject.toString();
        }
        return "";
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        monthOfYear = monthOfYear + 1;
        userDate = new StructDate();
        userDate.setYear(year);
        userDate.setMonth(monthOfYear);
        userDate.setDay(dayOfMonth);
        showDialogTime();
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        String currDate = userDate.getYear() + " / " + userDate.getMonth() + " / " + userDate.getDay();
        tvDate.setText(currDate + " | " + hourOfDay + " : " + minute);
        strUserDate = tvDate.getText().toString();
        userDate.setHourInDay(hourOfDay);
        userDate.setMinute(minute);
    }
}
