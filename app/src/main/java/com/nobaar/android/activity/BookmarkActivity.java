package com.nobaar.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.adapter.AddressHelperAdapter;
import com.nobaar.android.db.StructAddressHelper;
import com.nobaar.android.global.GlobalFunction;

import java.util.ArrayList;
import java.util.List;

public class BookmarkActivity extends AppCompatActivity implements AddressHelperAdapter.AddressHelperAdapterHandler, View.OnClickListener {
    public static final int REQUEST_CODE_ADDRESS_HELPER = 30;
    public static final String TAG_ADDRESS = "address";
    public static final String TAG_LAT = "lat";
    public static final String TAG_LNG = "lng";
    private RecyclerView mRecyclerView;
    private AddressHelperAdapter mAdapter;
    private List<StructAddressHelper> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);
        initVariable();
        initList();
        initHeader();
    }

    private void initHeader() {
        ((TextView) findViewById(R.id.include_app_bar_tv_page_name)).setText(R.string.bookmark_address);
        ImageView ivBack = findViewById(R.id.include_app_bar_iv_back);
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setOnClickListener(this);
    }

    private void initVariable() {
        mRecyclerView = findViewById(R.id.activity_bookmark_recycler);
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());

    }

    private void initList() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        dataList = StructAddressHelper.getAll();
        mAdapter = new AddressHelperAdapter(this, dataList);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemAddressClicked(int position) {
        Intent intent = new Intent();
        //   intent.putExtra(ContactsFragment.TAG_ADDRESS, s);
        intent.putExtra(TAG_LAT, dataList.get(position).getLatitude());
        intent.putExtra(TAG_LNG, dataList.get(position).getLongitude());
        setResult(REQUEST_CODE_ADDRESS_HELPER, intent);
        finish();
    }

    @Override
    public void onItemRemoveBookmarkClicked(int position) {
        StructAddressHelper structAddressHelper = dataList.get(position);
        if (structAddressHelper.delete()) {
            dataList.remove(position);
            mAdapter.notifyItemRemoved(position);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(0);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.include_app_bar_iv_back:
                onBackPressed();
                break;

        }
    }
}
