package com.nobaar.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nobaar.android.R;
import com.nobaar.android.global.AppPreferences;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.network.NetworkEventListener;
import com.nobaar.android.network.OkHttpHandler;

import org.json.JSONException;
import org.json.JSONObject;

public class UserDefinitionActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etName, etFamily;
    private ImageView ivConfirm;
    private ProgressBar pbConfirm;
    private AppPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_definition);

        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        preferences = AppPreferences.getInstance();
        etName = findViewById(R.id.activity_user_definition_et_name);
        etFamily = findViewById(R.id.activity_user_definition_et_family);
        pbConfirm = findViewById(R.id.user_definition_pb_bg_next);
        ivConfirm = findViewById(R.id.user_definition_iv_bg_next);
        ivConfirm.setOnClickListener(this);
        ivConfirm.setEnabled(false);

        findViewById(R.id.activity_user_definition_iv_back).setOnClickListener(this);
        findViewById(R.id.user_definition_fl_bg_next).setOnClickListener(this);

        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (etFamily.getText().toString().trim().length() > 0 && charSequence.toString().trim().length() > 0) {
                    ivConfirm.setEnabled(true);
                    ivConfirm.setImageResource(R.drawable.ic_okay);
                } else {
                    ivConfirm.setEnabled(false);
                    ivConfirm.setImageResource(R.drawable.ic_okay_deactive);
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etFamily.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (etName.getText().toString().trim().length() > 0 && charSequence.toString().trim().length() > 0) {
                    ivConfirm.setEnabled(true);
                    ivConfirm.setImageResource(R.drawable.ic_okay);

                } else {
                    ivConfirm.setEnabled(false);
                    ivConfirm.setImageResource(R.drawable.ic_okay_deactive);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_user_definition_iv_back:
                onBackPressed();
                break;
            case R.id.user_definition_iv_bg_next:
                if (validation())
                    callApi();
                break;
        }
    }

    private boolean validation() {
        final String name = etName.getText().toString().trim();
        final String family = etFamily.getText().toString().trim();
        if (name.length() == 0) {
            GlobalFunction.getInstance().toast(this, getString(R.string.enter_your_name));
            return false;
        }
        return true;
    }

    private void callApi() {
        final String name = etName.getText().toString().trim();
        final String family = etFamily.getText().toString().trim();

        ivConfirm.setVisibility(View.INVISIBLE);
        pbConfirm.setVisibility(View.VISIBLE);

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {

                    pbConfirm.setVisibility(View.INVISIBLE);
                    ivConfirm.setVisibility(View.VISIBLE);
                    preferences.setUserFamily(family);
                    preferences.setUserName(name);
                    GlobalFunction.getInstance().toast(UserDefinitionActivity.this, object.getString(Constants.JSON_MESSAGE));
                    startActivity(new Intent(UserDefinitionActivity.this, HomeActivity.class));
                    finish();
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                pbConfirm.setVisibility(View.INVISIBLE);
                ivConfirm.setVisibility(View.VISIBLE);
                GlobalFunction.getInstance().toast(UserDefinitionActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_SEND_PROFILE, eventListener)
                .addParam(Constants.JSON_MOBILE, AppPreferences.getInstance().getUserPhoneNumber())
                .addParam(Constants.JSON_NAME, name)
                .addParam(Constants.JSON_FAMILY, family)
                .send();

    }
}
