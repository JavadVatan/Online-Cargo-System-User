package com.nobaar.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.component.CodePicker;
import com.nobaar.android.global.AppPreferences;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.network.NetworkEventListener;
import com.nobaar.android.network.OkHttpHandler;

import org.json.JSONException;
import org.json.JSONObject;

public class ActivationActivity extends AppCompatActivity implements CodePicker.CodePickerHandler,
        View.OnClickListener {

    public static String result = "";
    private EditText code;
    private Button active;
    private TextView tvTitle;
    private CodePicker codePicker;
    private FrameLayout flConfirm;
    private ImageView ivConfirm;
    private ProgressBar pbConfirm;
    private AppPreferences preferences;
    private TextView tvSendCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activation);

        intiVariable();

    }

    private void intiVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        preferences = AppPreferences.getInstance();

        codePicker = findViewById(R.id.activity_activation_code_picker);
        flConfirm = findViewById(R.id.activity_activation_fl_confirm);
        pbConfirm = findViewById(R.id.activity_activation_pb_confirm);
        ivConfirm = findViewById(R.id.activity_activation_iv_confirm);
        tvTitle = findViewById(R.id.activity_activation_tv_tiltle);
        codePicker.setListener(this);
        flConfirm.setOnClickListener(this);
        tvSendCode = findViewById(R.id.activity_activation_tv_send_code);
        tvSendCode.setOnClickListener(this);
        findViewById(R.id.activity_activation_iv_back).setOnClickListener(this);
        tvTitle.setText(String.format(getString(R.string.enter_code_description),
                preferences.getUserPhoneNumber()));
        //   codePicker.
    }

    @Override
    public void codePickerStatus(boolean status) {
        flConfirm.setEnabled(status);
        if (status) {
            flConfirm.setBackground(getResources().getDrawable(R.drawable.bg_button_blue));
            GlobalFunction.getInstance().closeKeyboard(this);
            callApiActivation(codePicker.getCode());
        } else
            flConfirm.setBackground(getResources().getDrawable(R.drawable.bg_button));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_activation_fl_confirm:
                callApiActivation(codePicker.getCode());
                break;
            case R.id.activity_activation_tv_send_code:
                callApiSendCodeAgain(preferences.getUserPhoneNumber());
                break;
            case R.id.activity_activation_iv_back:
                onBackPressed();
                break;
        }
    }


    private void callApiActivation(final String code) {
        flConfirm.setVisibility(View.INVISIBLE);
        pbConfirm.setVisibility(View.VISIBLE);
        String phoneNumber = preferences.getUserPhoneNumber();

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    flConfirm.setVisibility(View.VISIBLE);
                    pbConfirm.setVisibility(View.INVISIBLE);

                    JSONObject dataObject = object.getJSONObject(Constants.JSON_DATA);

                    preferences.setUserId(dataObject.getString(Constants.JSON_USER_ID_LOWER_CASE));
                    preferences.setUserLogin(true);
                    preferences.setIsFirst(dataObject.getString(Constants.JSON_IS_FIRST));

                    if (dataObject.getString(Constants.JSON_IS_FIRST).equals(Constants.JSON_HAS_DATA)) {
                        preferences.setUserName(dataObject.getString(Constants.JSON_NAME));
                        preferences.setUserFamily(dataObject.getString(Constants.JSON_FAMILY));
                        preferences.setUserCredit(dataObject.getString(Constants.JSON_MONEY_BAG));

                        if (!dataObject.getString(Constants.JSON_HAVE_TRAVEL).equals(Constants.JSON_HAS_DATA))
                            startActivity(new Intent(ActivationActivity.this, HomeActivity.class));

                    } else {
                        if (!dataObject.getString(Constants.JSON_HAVE_TRAVEL).equals(Constants.JSON_HAS_DATA))
                            startActivity(new Intent(ActivationActivity.this, UserDefinitionActivity.class));

                    }

                    if (dataObject.getString(Constants.JSON_HAVE_TRAVEL).equals(Constants.JSON_HAS_DATA)) {
                        startActivity(new Intent(ActivationActivity.this,
                                RequestActivity.class));
                    }

                    GlobalFunction.getInstance().toast(ActivationActivity.this, object.getString(Constants.JSON_MESSAGE));
                    //   startActivity(new Intent(ActivationActivity.this, HomeActivity.class));
                    finish();

                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                flConfirm.setVisibility(View.VISIBLE);
                pbConfirm.setVisibility(View.INVISIBLE);
                GlobalFunction.getInstance().toast(ActivationActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_ACTIVATION + phoneNumber + "/" + code, eventListener)
                .send();
    }

    private void callApiSendCodeAgain(final String phone) {
        tvSendCode.setEnabled(false);

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200) ||
                        object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_402)) {

                    tvSendCode.setEnabled(true);
                    GlobalFunction.getInstance().toast(ActivationActivity.this,
                            getString(R.string.toast_send_code_again));


                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                tvSendCode.setEnabled(true);
                GlobalFunction.getInstance().toast(ActivationActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_LOGIN + phone, eventListener)
                .send();
    }

}

