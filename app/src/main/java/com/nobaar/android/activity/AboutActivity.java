package com.nobaar.android.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.global.GlobalFunction;


public class AboutActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvAbout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        initVariable();
    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        tvAbout = findViewById(R.id.activity_about_tv_about);
        findViewById(R.id.activity_about_iv_back).setOnClickListener(this);
        tvAbout.setText(Html.fromHtml(getString(R.string.about_description)));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_about_iv_back:
                onBackPressed();
                break;
        }
    }
}
