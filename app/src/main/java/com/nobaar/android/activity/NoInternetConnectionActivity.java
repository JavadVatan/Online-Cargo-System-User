package com.nobaar.android.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.nobaar.android.Application;
import com.nobaar.android.R;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;


public class NoInternetConnectionActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvTryAgain, tvCheckNetwork;
    private GlobalFunction globalFunction = GlobalFunction.getInstance();
    private Uri currUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet_connection);
        initBundle();
        initVariable();
    }

    private void initBundle() {
        if (getIntent() != null)
            currUri = getIntent().getData();

    }

    private void initVariable() {
        tvCheckNetwork = (TextView) findViewById(R.id.internet_connection_tv_check_network_setting);
        tvTryAgain = (TextView) findViewById(R.id.no_internet_connection_tv_try_again);
        ((TextView) findViewById(R.id.no_internet_connection_tv_error)).setTypeface(Constants.iranSenseBold);

        tvCheckNetwork.setTypeface(Constants.iranSenseLight);
        tvTryAgain.setTypeface(Constants.iranSenseLight);
        tvTryAgain.setOnClickListener(this);
        tvCheckNetwork.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.internet_connection_tv_check_network_setting:
                globalFunction.openNetworkSetting(this);
                break;
            case R.id.no_internet_connection_tv_try_again:
                if (Application.getInstance().isConnectionAnyInternet()) {
                    manageTryAgain();
                }

                break;
        }
    }

    private void manageTryAgain() {
        if (currUri != null) {
            Intent i = new Intent(Intent.ACTION_VIEW, currUri);
            startActivity(i);
        } else {
            onBackPressed();
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        if (Application.getInstance().isConnectionAnyInternet()) {
            super.onBackPressed();
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
        }
    }
}
