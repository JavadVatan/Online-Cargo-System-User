package com.nobaar.android.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.adapter.RequestAdapter;
import com.nobaar.android.dialog.DriverInfoDialog;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.global.TemporaryPreferences;
import com.nobaar.android.network.NetworkEventListener;
import com.nobaar.android.network.OkHttpHandler;
import com.nobaar.android.struct.StructDriverInfo;
import com.nobaar.android.struct.StructRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Javad Vatan on 10/23/2017.
 */

public class RequestActivity extends AppCompatActivity implements View.OnClickListener,
        RequestAdapter.RequestAdapterHandler {

    private RecyclerView mRecyclerView;
    private RequestAdapter mAdapter;
    private List<StructRequest> dataList = new ArrayList<>();
    private TemporaryPreferences prefTemp;
    private StructDriverInfo driverInfo;
    private FrameLayout flConfirm;
    private TextView tvConfirm;
    private ProgressBar pbConfirm;
    private String remainTime;

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);
        initVariable();
        initHeader();
        setOnClick();
        initList();
        callApiGetOrderStatus();
        setStatus1();
        mAdapter.notifyDataSetChanged();
        //getData();
    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        prefTemp = TemporaryPreferences.getInstance();
        mRecyclerView = findViewById(R.id.activity_request_recycler);
        flConfirm = findViewById(R.id.activity_request_fl_confirm);
        tvConfirm = findViewById(R.id.activity_request_tv_confirm);
        pbConfirm = findViewById(R.id.activity_request_pb_confirm);
    }

    private void initHeader() {
        ((TextView) findViewById(R.id.include_app_bar_tv_page_name))
                .setText("#" + prefTemp.getPrefString(TemporaryPreferences.PREF_ORDER_ID, ""));
     /*   ImageView ivBack = findViewById(R.id.include_app_bar_iv_back);
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setOnClickListener(this);*/
    }

    private void setOnClick() {
        int[] viewId = new int[]{R.id.include_app_bar_iv_share, R.id.include_app_bar_iv_delete};
        for (int aViewId : viewId) {
            View view = findViewById(aViewId);
            view.setOnClickListener(this);
            view.setVisibility(View.VISIBLE);
        }
        flConfirm.setOnClickListener(this);
    }

    private void initList() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new RequestAdapter(this, dataList);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.include_app_bar_iv_back:
                onBackPressed();
                break;

            case R.id.include_app_bar_iv_share:
                shareOrder();
                break;

            case R.id.include_app_bar_iv_delete:
                callApiCancelOrder();
                break;

            case R.id.activity_request_fl_confirm:
                manageFactor(FactorActivity.TAG_MODE_FINAL);
                break;
        }
    }

    private void shareOrder() {
        // TODO: 11/20/2017 Added share link here
    }

    private void callApiCancelOrder() {

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {

                    GlobalFunction.getInstance().toast(RequestActivity.this,
                            object.getString(Constants.JSON_MESSAGE));
                    manageCancelOrder();
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                GlobalFunction.getInstance().toast(RequestActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_CANCEL_ORDER, eventListener)
                .addParam(Constants.JSON_ORDER_ID, prefTemp.getPrefString(
                        TemporaryPreferences.PREF_ORDER_ID, ""))
                .send();
    }

    private void manageCancelOrder() {
        prefTemp.clearPreferences();
        Intent intent = new Intent(RequestActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void callApiGetOrderStatus() {

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {

                    JSONObject jsonObject = object.getJSONObject(Constants.JSON_DATA);
                    if (jsonObject.has(Constants.JSON_TIME_CAL))
                        remainTime = jsonObject.getString(Constants.JSON_TIME_CAL);

                    if (jsonObject.has(Constants.JSON_DRIVER))
                        parseDriverData(jsonObject.getJSONArray(Constants.JSON_DRIVER));

                    manageStatus(Integer.parseInt(jsonObject.getString(Constants.JSON_STATUS)));

                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                if (string != null && string.length() > 0 && !string.equals("اطلاعاتی موجود نیست"))
                    GlobalFunction.getInstance().toast(RequestActivity.this, string);
            }
        };

        String url = Constants.API_GET_ORDER_STATUS + prefTemp.getPrefString(
                TemporaryPreferences.PREF_ORDER_ID, "");

        new OkHttpHandler(this, url, eventListener)
                .send();
    }

    private void parseDriverData(JSONArray jsonArray) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {
            driverInfo = new StructDriverInfo(
                    jsonArray.getJSONObject(i).getString(Constants.JSON_DRIVER_NAME) +
                            jsonArray.getJSONObject(i).getString(Constants.JSON_DRIVER_FAMILY),
                    jsonArray.getJSONObject(i).getString(Constants.JSON_DRIVER_CAR),
                    jsonArray.getJSONObject(i).getString(Constants.JSON_DRIVER_RANK),
                    jsonArray.getJSONObject(i).getString(Constants.JSON_DRIVER_MOBILE),
                    jsonArray.getJSONObject(i).getString(Constants.JSON_DRIVER_CAR_PIC));
        }

        if (driverInfo != null) {
            prefTemp.addPrefString(TemporaryPreferences.PREF_DRIVER_NAME, driverInfo.getDriverName());
            prefTemp.addPrefString(TemporaryPreferences.PREF_DRIVER_CAR_NAME, driverInfo.getCarName());
            prefTemp.addPrefString(TemporaryPreferences.PREF_DRIVER_PIC, driverInfo.getPhotoUrl());
            //   prefTemp.addPrefString(TemporaryPreferences.PREF_DRIVER_ID,driverInfo.getPhotoUrl());
        }
    }

    private void manageStatus(int status) {
        switch (status) {
            case Constants.STATUS_ORDER_REGISTERED:
            case Constants.STATUS_DRIVER_CANCEL_ORDER:
                setStatus1();
                break;
            case Constants.STATUS_DRIVER_ACCEPT_ORDER:
                setStatus2();
                break;
            case Constants.STATUS_DRIVER_ACCEPT_DETAIL_ORDER:
                // setStatus3();
                setStatus2();

                break;
            case Constants.STATUS_DRIVER_START_PROCESS:
                setStatus4();
                break;

            case Constants.STATUS_DRIVER_END_PROCESS:
                setStatus5();
                break;
        }
        mAdapter.notifyDataSetChanged();
    }

    private void setStatus1() {
        dataList.clear();
        findViewById(R.id.include_app_bar_iv_delete).setVisibility(View.VISIBLE);
        flConfirm.setVisibility(View.INVISIBLE);
        String title = getString(R.string.request_has_been_registered);
        String detail = "کد درخواست شما: " + prefTemp.getPrefString(TemporaryPreferences.PREF_ORDER_ID, "");
        dataList.add(new StructRequest(title, detail, R.drawable.ic_down, R.drawable.bg_circle_white,
                R.drawable.ic_show));

        dataList.add(new StructRequest(getString(R.string.we_checking_your_request), null,
                R.drawable.ic_history, R.drawable.bg_circle_blue,
                R.drawable.ic_refresh));
    }

    private void setStatus2() {
        setStatus1();
        findViewById(R.id.include_app_bar_iv_delete).setVisibility(View.GONE);
        String title = driverInfo.getDriverName() + " " + getString(R.string.accept_your_request);
        String detail = getString(R.string.wait_for_driver);
        dataList.set(1, new StructRequest(title, detail, R.drawable.ic_down, R.drawable.bg_circle_white,
                R.drawable.ic_show));

 /*       dataList.add(new StructRequest(getString(R.string.wait_for_accept_detail_your_request),
                getString(R.string.description_for_accept_detail_request),
                R.drawable.ic_chek_detail, R.drawable.bg_circle_blue,
                R.drawable.ic_refresh));
*/
        dataList.add(new StructRequest(getString(R.string.title_step4),
                null,
                R.drawable.ic_car, R.drawable.bg_circle_blue,
                0));

    }

    private void setStatus3() {
        setStatus1();
        setStatus2();
        //flConfirm.setVisibility(View.VISIBLE);

        String title = getString(R.string.title_step_3);
        String detail = getString(R.string.detail_step3);
        dataList.set(2, new StructRequest(title, detail, R.drawable.ic_pen, R.drawable.bg_circle_white,
                R.drawable.ic_show));

        dataList.add(new StructRequest(getString(R.string.title_step4),
                null,
                R.drawable.ic_car, R.drawable.bg_circle_blue,
                0));
    }

    private void setStatus4() {
        setStatus1();
        setStatus2();
        //flConfirm.setVisibility(View.VISIBLE);

        /*String title = getString(R.string.title_step_3);
        String detail = getString(R.string.detail_step3);
        dataList.set(2, new StructRequest(title, detail, R.drawable.ic_pen, R.drawable.bg_circle_white,
                R.drawable.ic_show));
*/
        dataList.set(2, new StructRequest(getString(R.string.carring),
                remainTime,
                R.drawable.ic_car, R.drawable.bg_circle_blue,
                0, true));
    }

    private void setStatus5() {
        setStatus1();
        setStatus2();
        setStatus4();
        flConfirm.setVisibility(View.VISIBLE);
        flConfirm.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.MULTIPLY);

        tvConfirm.setText(R.string.show_factor);

        int remainIntTime = Math.abs(Integer.parseInt(remainTime));
        @SuppressLint("DefaultLocale") String timeFormat = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(remainIntTime * 1000),
                TimeUnit.MILLISECONDS.toMinutes(remainIntTime * 1000) % 60,
                TimeUnit.MILLISECONDS.toSeconds(remainIntTime * 1000) % 60);
        dataList.set(2, new StructRequest(getString(R.string.step_four_fourth_title),
                timeFormat,
                R.drawable.ic_down, R.drawable.bg_circle_white,
                0, false));

    }

    @Override
    public void onItemClicked(int position) {
        switch (position) {
            case 0:
                manageFactor(FactorActivity.TAG_MODE_REVIEW);
                break;
            case 1:
                showDialogDriver();
                break;
        }
    }


    private void manageFactor(int type) {
        Intent intent = new Intent(this, FactorActivity.class);
        intent.putExtra(FactorActivity.TAG_MODE, type);
        startActivity(intent);
    }

    private void showDialogDriver() {
        new DriverInfoDialog(this, driverInfo)
                .setCancelableDialog(true, null)
                .setCanceledOnTouchOutsideDialog(true)
                .show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        callApiGetOrderStatus();
    }

}
