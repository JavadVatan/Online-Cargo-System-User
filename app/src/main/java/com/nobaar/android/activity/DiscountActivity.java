package com.nobaar.android.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.nobaar.android.R;
import com.nobaar.android.global.AppPreferences;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.network.NetworkEventListener;
import com.nobaar.android.network.OkHttpHandler;

import org.json.JSONException;
import org.json.JSONObject;


public class DiscountActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvDiscount, tvShare;
    private String currDiscountCode = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount);
        initVariable();
        getDiscountCode();
    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        tvDiscount = findViewById(R.id.activity_discount_code_tv_code);
        findViewById(R.id.activity_discount_code_iv_back).setOnClickListener(this);
        findViewById(R.id.activity_discount_code_iv_share).setOnClickListener(this);
        findViewById(R.id.activity_discount_code_iv_copy).setOnClickListener(this);
    }


    private void getDiscountCode() {
        String url = Constants.API_GET_SHARE_CODE + AppPreferences.getInstance().getUserPhoneNumber();
        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {

                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    tvDiscount.setText("کد : " + object.getString(Constants.JSON_SHARE_CODE));
                    currDiscountCode = object.getString(Constants.JSON_SHARE_CODE);
                } else
                    onError(object.getString(Constants.JSON_MESSAGE));
            }

            public void onError(String string) {
                tvDiscount.setText("آماده سازی کد با مشکل مواجه گردید.");

                GlobalFunction.getInstance().toast(DiscountActivity.this, string);
            }

        };

        new OkHttpHandler(this, url, eventListener)
                .send();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_discount_code_iv_copy:
                manageCopy();
                break;
            case R.id.activity_discount_code_iv_share:
                manageShare();
                break;
            case R.id.activity_discount_code_iv_back:
                onBackPressed();
                break;
        }
    }

    private void manageShare() {
        if (currDiscountCode != null) {
            GlobalFunction.getInstance().shareText(this,
                    getString(R.string.share_discount_code_first_description) +
                            currDiscountCode + " " +
                            getString(R.string.share_discount_code_second_description), "سفر رایگان نوبار");
        } else
            Toast.makeText(this, R.string.toast_un_ready_discount_code, Toast.LENGTH_SHORT).show();

    }

    private void manageCopy() {
        if (currDiscountCode != null) {
            GlobalFunction.getInstance().copyTextIntoCliboard(this, getString(R.string.copy_description)
                    , tvDiscount.getText().toString());
            Toast.makeText(this, R.string.copied, Toast.LENGTH_SHORT).show();

        } else
            Toast.makeText(this, R.string.toast_un_ready_discount_code, Toast.LENGTH_SHORT).show();

    }
}
