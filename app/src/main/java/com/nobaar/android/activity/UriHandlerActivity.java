package com.nobaar.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.nobaar.android.R;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.global.TemporaryPreferences;

/**
 * Created by Javad Vatan on 2/1/2018.
 */

public class UriHandlerActivity extends AppCompatActivity {
    private TemporaryPreferences pref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = TemporaryPreferences.getInstance();

        String currRequest = pref.getPrefString(TemporaryPreferences.PREF_BANK_REQUEST, "");

        switch (currRequest) {
            case TemporaryPreferences.PREF_BANK_REQUEST_PAY_COST:
                manageClass(RecordRating.class);
                break;
            case TemporaryPreferences.PREF_BANK_REQUEST_WALLET:
                manageClass(AccountancyActivity.class);
                break;
        }
    }

    private void manageClass(Class aClass) {
        GlobalFunction.getInstance().toast(this,getString(R.string.pay_with_succsess));
        pref.addPrefString(TemporaryPreferences.PREF_BANK_REQUEST, "");
        Intent intent = new Intent(this, aClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
