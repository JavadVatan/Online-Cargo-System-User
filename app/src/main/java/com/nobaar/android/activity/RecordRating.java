package com.nobaar.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.global.TemporaryPreferences;
import com.nobaar.android.network.NetworkEventListener;
import com.nobaar.android.network.OkHttpHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecordRating extends AppCompatActivity implements View.OnClickListener {
    private CircleImageView ivAvatar;
    private TextView tvDriverName, tvDriverCar;
    private TemporaryPreferences preferences;
    private RatingBar rbDriverRate;
    private FrameLayout flConfirm;
    private TextView tvConfirm;
    private ProgressBar pbConfirm;
    private boolean isFromServer = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_racord_rating);
        initializer();
    }

    private void initializer() {
        initBundle();
        initVariable();
        setData();
    }

    private void initBundle() {
        if (getIntent().getData() != null) {
            isFromServer = true;
            GlobalFunction.getInstance().toast(this, getString(R.string.success_payment));
        }
    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        preferences = TemporaryPreferences.getInstance();
        ivAvatar = findViewById(R.id.activity_record_rank_iv_driver_icon);
        tvDriverName = findViewById(R.id.activity_record_rank_tv_driver_name);
        tvDriverCar = findViewById(R.id.activity_record_rank_tv_driver_car);
        rbDriverRate = findViewById(R.id.dialog_get_comment_rb_seller_behavior);
        flConfirm = findViewById(R.id.activity_rate_fl_confirm);
        tvConfirm = findViewById(R.id.activity_rate_tv_confirm);
        pbConfirm = findViewById(R.id.activity_rate_pb_confirm);
        flConfirm.setOnClickListener(this);
    }

    private void setData() {
        if (!preferences.getPrefString(TemporaryPreferences.PREF_DRIVER_PIC, "").equals("")) {
            Picasso.with(this).load(preferences.getPrefString(TemporaryPreferences.PREF_DRIVER_PIC, ""))
                    .placeholder(R.drawable.ic_avatar)
                    .into(ivAvatar);
        }
        tvDriverName.setText(preferences.getPrefString(TemporaryPreferences.PREF_DRIVER_NAME, ""));
        tvDriverCar.setText(preferences.getPrefString(TemporaryPreferences.PREF_DRIVER_CAR_NAME, ""));
    }

    @Override
    public void onClick(View view) {
        callApiAddRank();
    }

    private void callApiAddRank() {
        tvConfirm.setVisibility(View.INVISIBLE);
        pbConfirm.setVisibility(View.VISIBLE);
        NetworkEventListener eventListener = new NetworkEventListener() {

            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                pbConfirm.setVisibility(View.INVISIBLE);
                tvConfirm.setVisibility(View.VISIBLE);
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    GlobalFunction.getInstance().toast(RecordRating.this, object.getString(Constants.JSON_MESSAGE));
                    manageFinishOrder();
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            @Override
            public void onError(String string) {
                pbConfirm.setVisibility(View.INVISIBLE);
                tvConfirm.setVisibility(View.VISIBLE);
                GlobalFunction.getInstance().toast(RecordRating.this, string);
            }
        };

        int rate = rbDriverRate.getNumStars();
        new OkHttpHandler(this, Constants.API_ADD_RANK, eventListener)
                .addParam(Constants.JSON_DRIVER_ID, "1")/*preferences.getPrefString(TemporaryPreferences.PREF_DRIVER_ID))*/
                .addParam(Constants.JSON_RANK, rate + "")
                .addParam("comment", "comment")
                .send();
    }

    private void manageFinishOrder() {
        TemporaryPreferences.getInstance().clearPreferences();
        Intent intent = new Intent(RecordRating.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
