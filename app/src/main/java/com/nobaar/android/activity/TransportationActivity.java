package com.nobaar.android.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.adapter.TransportationAdapter;
import com.nobaar.android.global.AppPreferences;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.network.NetworkEventListener;
import com.nobaar.android.network.OkHttpHandler;
import com.nobaar.android.struct.StructTransportation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TransportationActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView mRecyclerView;
    private TransportationAdapter mAdapter;
    private List<StructTransportation> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_list_page);
        initVariable();
        initHeader();
        initList();
        getData();
    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        mRecyclerView = findViewById(R.id.base_list_page_recycler);
    }

    private void initHeader() {
        ((TextView) findViewById(R.id.include_app_bar_tv_page_name)).setText(R.string.transportaion);
        ImageView ivBack = findViewById(R.id.include_app_bar_iv_back);
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setOnClickListener(this);
    }

    private void initList() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new TransportationAdapter(dataList);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getData() {
        String str = Constants.API_GET_ORDER + AppPreferences.getInstance().getUserId();

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                findViewById(R.id.base_list_pb_recycler).setVisibility(View.GONE);

                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    JSONArray objectArray = object.getJSONArray(Constants.JSON_DATA);
                    dataList.clear();

                    for (int i = 0; i < objectArray.length(); i++) {
                        JSONObject jsonObject = objectArray.getJSONObject(i);
                        StructTransportation structTransportation = new StructTransportation();
                        structTransportation.setOrderName(jsonObject.getString(Constants.JSON_ID));
                        structTransportation.setOrderDate(jsonObject.getString(Constants.JSON_TIME));
                        dataList.add(structTransportation);
                    }
                    if (dataList.size() == 0) {
                        findViewById(R.id.base_list_tv_recycler).setVisibility(View.VISIBLE);
                    }
                    mAdapter.notifyDataSetChanged();
                } else
                    onError(object.getString(Constants.JSON_MESSAGE));
            }

            public void onError(String string) {
                GlobalFunction.getInstance().toast(TransportationActivity.this, string);
            }

        };

        new OkHttpHandler(this, str, eventListener)
                .send();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.include_app_bar_iv_back:
                onBackPressed();
                break;
        }

    }

}
