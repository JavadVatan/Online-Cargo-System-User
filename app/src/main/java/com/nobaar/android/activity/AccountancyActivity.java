package com.nobaar.android.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nobaar.android.R;
import com.nobaar.android.global.AppPreferences;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.global.TemporaryPreferences;
import com.nobaar.android.network.NetworkEventListener;
import com.nobaar.android.network.OkHttpHandler;
import com.nobaar.android.util.NumberTextWatcherForThousand;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;


public class AccountancyActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String KEY_BANK_URL = "bank_url_with_token";
    private AppPreferences preferences;
    private ImageView ivConfirm;
    private ProgressBar pbConfirm;
    private EditText etAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountancy);
        initVariable();
        initHeader();
        getCredit();
    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        preferences = AppPreferences.getInstance();
        pbConfirm = findViewById(R.id.activity_accountancy_pb_confirm);
        ivConfirm = findViewById(R.id.activity_accountancy_iv_confirm);
        etAmount = findViewById(R.id.activity_et_amount);
        ivConfirm.setOnClickListener(this);
        new NumberTextWatcherForThousand(etAmount);
    }

    private void initHeader() {
        int[] headerId = new int[]{R.id.accountancy_activity_iv_back};
        for (int aHeaderId : headerId) {
            View view = findViewById(aHeaderId);
            view.setVisibility(View.VISIBLE);
            view.setOnClickListener(this);
        }
    }

    private void callApiSendToBank(String amount) {
        amount = amount.replaceAll(",", "");
        ivConfirm.setVisibility(View.INVISIBLE);
        pbConfirm.setVisibility(View.VISIBLE);
        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    pbConfirm.setVisibility(View.INVISIBLE);
                    ivConfirm.setVisibility(View.VISIBLE);

                    if (Objects.equals(object.getJSONObject(Constants.JSON_DATA)
                            .getString(Constants.JSON_CODE), Constants.PAYMENT_IS_READY)) {

                        manageApiSendToBank(object.getJSONObject(Constants.JSON_DATA)
                                .getString(Constants.JSON_TOKEN));
                    } else {
                        onError(object.getString(Constants.JSON_MESSAGE));
                    }
                } else {
                    pbConfirm.setVisibility(View.INVISIBLE);
                    ivConfirm.setVisibility(View.VISIBLE);
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                GlobalFunction.getInstance().toast(AccountancyActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_SEND_TO_BANK_WALLET, eventListener)
                .addParam(Constants.JSON_TYPE, "mobile")
                .addParam(Constants.JSON_AMOUNT, amount)
                .addParam(Constants.JSON_USER_ID_LOWER_CASE, preferences.getUserId())
                .send();
    }

    private void manageApiSendToBank(String token) {
        String url = Constants.API_BANK + token;
        Intent intent = new Intent(this, PayActivity.class);
        intent.putExtra(KEY_BANK_URL, url);
        startActivity(intent);
    }

    @SuppressLint("StaticFieldLeak")
    private void getCredit() {
        NetworkEventListener eventListener = new NetworkEventListener() {

            @Override
            public void onSuccess(JSONObject object) {
                try {
                    if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                        String credit = object.getString(Constants.JSON_AMOUNT);
                        preferences.setUserCredit(credit);
                        GlobalFunction.getInstance().toast(AccountancyActivity.this, String.format("مانده حساب %s ریال ", credit));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String string) {
            }
        };

        new OkHttpHandler(this, Constants.API_SEND_GET_CREDIT, eventListener)
                .addParam(Constants.JSON_USER_ID_LOWER_CASE, preferences.getUserId())
                .send();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.accountancy_activity_iv_back:
                onBackPressed();
                break;
            case R.id.activity_accountancy_iv_confirm:
                manageConfirm();
                break;

        }
    }

    private void manageConfirm() {
        if (etAmount.getText().length() >= 4) {
            TemporaryPreferences pref = TemporaryPreferences.getInstance();
            pref.addPrefString(TemporaryPreferences.PREF_BANK_REQUEST, TemporaryPreferences.PREF_BANK_REQUEST_WALLET);
            callApiSendToBank(etAmount.getText().toString());
        } else
            etAmount.setError(getString(R.string.min_amount));
    }

}
