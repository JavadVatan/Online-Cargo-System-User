package com.nobaar.android.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.global.GlobalFunction;

public class PayActivity extends AppCompatActivity {
    private WebView wvBrowser;
    private String bankUrl;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_activity);
        Intent i = getIntent();
        bankUrl = i.getStringExtra(FactorActivity.KEY_BANK_URL);
        initVariable();

        startWebView(bankUrl);
    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        wvBrowser = findViewById(R.id.pay_webview);
        //wvBrowser.getSettings().setJavaScriptEnabled(true);

        ((TextView) findViewById(R.id.include_app_bar_tv_page_name)).setText(R.string.payement_online);
    }

    private void startWebView(String url) {
        wvBrowser.clearCache(true);
        wvBrowser.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains("exit_page")) {
                    onBackPressed();
                    return true;
                } else if (url.equals("nobaar://main")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                 //   intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    return true;
                }
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                if (errorCode == -2) {
                    String errorMessage = getString(R.string.error_bank_as_html);
                    view.loadData(errorMessage, "text/html", "utf-8");
                    return;
                }
                super.onReceivedError(view, errorCode, description, failingUrl);
            }


            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                           SslError error) {
                //super.onReceivedSslError(view, handler, error);
                handler.proceed();
            }

            //Show loader on url load
            public void onLoadResource(WebView view, String url) {
                if (progressDialog == null && !url.contains("/images/favicon.ico")) {
                    progressDialog = new ProgressDialog(PayActivity.this);
                    progressDialog.setMessage(getString(R.string.alert_wait));
                    progressDialog.show();
                }
            }

            public void onPageFinished(WebView view, String url) {
                try {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

        });
        // Do not disable this line , because it used when user will redirect to bank
        wvBrowser.getSettings().setDomStorageEnabled(true);
        wvBrowser.getSettings().setJavaScriptEnabled(true);
        wvBrowser.getSettings().setDefaultTextEncodingName("utf-8");
        wvBrowser.loadUrl(url);
    }

}
