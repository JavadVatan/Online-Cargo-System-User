package com.nobaar.android.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.global.GlobalFunction;

public class RuleActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvRules;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rule);
        initVariable();
    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        tvRules = findViewById(R.id.activity_rule_tv_rules);
        findViewById(R.id.activity_about_iv_back).setOnClickListener(this);
        tvRules.setText(Html.fromHtml(getString(R.string.rules)));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_about_iv_back:
                onBackPressed();
                break;
        }
    }
}
