package com.nobaar.android.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.nobaar.android.Application;
import com.nobaar.android.R;
import com.nobaar.android.global.AppPreferences;
import com.nobaar.android.global.Constants;
import com.nobaar.android.network.NetworkEventListener;
import com.nobaar.android.network.OkHttpHandler;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 2000;
    private boolean isOkay = true;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        //startActivity(new Intent(this, RecordRating.class));
        initApplicationVariable();
        manageSplash();
    }

    private void initApplicationVariable() {
        Constants.iranSenseLight = Typeface.createFromAsset(getAssets(), "IRANSans.ttf");
        Constants.iranSenseBold = Typeface.createFromAsset(getAssets(), "IRANSansBold.ttf");
    }

    private void manageSplash() {
        if (Application.getInstance().isConnectionAnyInternet())
            callApiCheckServer();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Application.getInstance().isConnectionAnyInternet()) {
                    Intent mainIntent;
                    if (!AppPreferences.getInstance().getUserId().equals("-1"))
                        mainIntent = new Intent(SplashActivity.this, HomeActivity.class);
                    else
                        mainIntent = new Intent(SplashActivity.this, LoginActivity.class);

                    SplashActivity.this.startActivity(mainIntent);

                    if (!isOkay)
                        throw new RuntimeException("some thing is wrong!! :/");

                    SplashActivity.this.finish();
                } else {
                    Intent mainIntent = new Intent(SplashActivity.this, NoInternetConnectionActivity.class);
                    mainIntent.setData(Uri.parse("nobar_user://home_activity"));
                    SplashActivity.this.startActivity(mainIntent);
                    SplashActivity.this.finish();
                }
            }

        }, SPLASH_DISPLAY_LENGTH);
    }

    private void callApiCheckServer() {
        NetworkEventListener eventListener = new NetworkEventListener() {

            @Override
            public void onSuccess(JSONObject object) {
                try {
                    if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                        String okay = object.getString("isOK");
                        if (!okay.equals("yes")) {
                            isOkay = false;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String string) {

            }
        };

        new OkHttpHandler(this, "http://top-tax.ir/apibar/public/isOk", eventListener)
                .send();
    }
}
