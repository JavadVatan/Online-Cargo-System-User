package com.nobaar.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nobaar.android.R;
import com.nobaar.android.global.AppPreferences;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.network.NetworkEventListener;
import com.nobaar.android.network.OkHttpHandler;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    private EditText etPhoneNumber;
    private ImageView ivLogin;
    private ProgressBar pbLogin;
    private AppPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //actionbar(0, 0);
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        preferences = AppPreferences.getInstance();
        pbLogin = findViewById(R.id.activity_login_pb_confirm);
        ivLogin = findViewById(R.id.activity_login_iv_confirm);
        etPhoneNumber = findViewById(R.id.activity_login_et_phone_number);


        ivLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation())
                    login(etPhoneNumber.getText().toString());
            }
        });
    }

    private boolean validation() {
        if (etPhoneNumber.getText().toString().length() == 0) {
            etPhoneNumber.setError(getString(R.string.error_empty_hone_number));
            return false;
        } else if (etPhoneNumber.getText().toString().length() != 11) {
            etPhoneNumber.setError(getString(R.string.error_uncorrect_phone_number));
            return false;
        }
        return true;
    }

    private void login(final String phone) {
        ivLogin.setVisibility(View.INVISIBLE);
        pbLogin.setVisibility(View.VISIBLE);

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200) ||
                        object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_402)) {

                    pbLogin.setVisibility(View.INVISIBLE);
                    ivLogin.setVisibility(View.VISIBLE);
                    preferences.setUserPhoneNumber(phone);
                    GlobalFunction.getInstance().toast(LoginActivity.this, object.getString(Constants.JSON_MESSAGE));

                    startActivity(new Intent(LoginActivity.this, ActivationActivity.class));
                    finish();
              /*      preferences.setUserId(object.getString(Constants.JSON_USER_ID));
                    preferences.setUserLogin(true);
                    preferences.setUserCredit(object.getString(Constants.JSON_USER_COST));
                    preferences.setUserFullName(object.getString(Constants.JSON_USER_FULL_NAME));*/

                    /*activation.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    activation.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);*/

                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                pbLogin.setVisibility(View.INVISIBLE);
                ivLogin.setVisibility(View.VISIBLE);
                GlobalFunction.getInstance().toast(LoginActivity.this, string);
            }

        };

        new OkHttpHandler(this, Constants.API_LOGIN + phone, eventListener)
                .send();

    }


}
