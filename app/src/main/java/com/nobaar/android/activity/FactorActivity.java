package com.nobaar.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.Util;
import com.nobaar.android.adapter.DetailFactorAdapter;
import com.nobaar.android.dialog.DiscountDialog;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.global.TemporaryPreferences;
import com.nobaar.android.network.NetworkEventListener;
import com.nobaar.android.network.OkHttpHandler;
import com.nobaar.android.struct.StructDetailFactor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Javad Vatan on 10/19/2017.
 */

public class FactorActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String KEY_BANK_URL = "bank_url_with_token";
    public static final String TAG_MODE = "mode_page";
    public static final int TAG_MODE_NORMAL = 0;
    public static final int TAG_MODE_REVIEW = 1;
    public static final int TAG_MODE_FINAL = 2;
    private TextView tvConfirm, tvPrice, tvAddressOrigin, tvAddressDestination, tvDate,
            tvReceiverProperty, tvDiscount;
    private RecyclerView rvDetailFactor;
    private TemporaryPreferences preferences;
    private ProgressBar pbPrice, pbConfirm, pbDiscount;
    private int currMode = TAG_MODE_NORMAL;
    private String finalPrice = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temporary_factor);
        initBundle();
        initVariable();
        initList();
        setDataText();
    }

    private void initBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            currMode = bundle.getInt(TAG_MODE);
    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        preferences = TemporaryPreferences.getInstance();
        rvDetailFactor = findViewById(R.id.activity_factor_rv_detail_factor);
        tvAddressOrigin = findViewById(R.id.activity_factor_tv_address_origin);
        tvAddressDestination = findViewById(R.id.activity_factor_tv_address_destination);
        tvDate = findViewById(R.id.activity_factor_tv_loading_date);
        tvReceiverProperty = findViewById(R.id.activity_factor_tv_receiver_property);
        tvPrice = findViewById(R.id.activity_factor_tv_price);
        pbPrice = findViewById(R.id.activity_factor_pb_price);
        tvDiscount = findViewById(R.id.activity_factor_tv_discount);
        pbDiscount = findViewById(R.id.activity_factor_pb_discount);
        pbConfirm = findViewById(R.id.activity_factor_pb_confirm);
        tvConfirm = findViewById(R.id.activity_factor_tv_confirm);
        tvConfirm.setOnClickListener(this);


        setPageModeText();
    }

    private void setPageModeText() {
        FrameLayout flPrice = findViewById(R.id.activity_factor_fl_price);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) tvPrice.getLayoutParams();
        LinearLayout.LayoutParams flParam = (LinearLayout.LayoutParams) flPrice.getLayoutParams();
        params.gravity = Gravity.CENTER;
        flParam.width = LinearLayout.LayoutParams.MATCH_PARENT;

        switch (currMode) {
            case TAG_MODE_NORMAL:
                ((TextView) findViewById(R.id.include_app_bar_tv_page_name)).setText(R.string.temporary_factor);
                ((TextView) findViewById(R.id.activity_factor_tv_confirm)).setText(R.string.register_request);
                tvPrice.setLayoutParams(params);
                pbPrice.setLayoutParams(params);
                flPrice.setLayoutParams(flParam);
                break;

            case TAG_MODE_REVIEW:
                ((TextView) findViewById(R.id.include_app_bar_tv_page_name)).setText(R.string.factor);
                ((TextView) findViewById(R.id.activity_factor_tv_confirm)).setText(R.string.back);
                tvPrice.setLayoutParams(params);
                pbPrice.setLayoutParams(params);
                flPrice.setLayoutParams(flParam);
                break;

            case TAG_MODE_FINAL:
                ((TextView) findViewById(R.id.include_app_bar_tv_page_name)).setText(R.string.factor_final);
                ((TextView) findViewById(R.id.activity_factor_tv_confirm)).setText(R.string.pay_with_credit);
                tvDiscount.setVisibility(View.VISIBLE);
                tvDiscount.setOnClickListener(this);

                break;
        }
    }

    private void setDataText() {
        tvAddressOrigin.setText(preferences.getPrefString(TemporaryPreferences.PREF_ADDRESS_ORIGIN, ""));
        tvAddressDestination.setText(preferences.getPrefString(TemporaryPreferences.PREF_ADDRESS_DESTINATION, ""));
        tvDate.setText(preferences.getPrefString(TemporaryPreferences.PREF_TIME_STRING, ""));

        String receiverName = preferences.getPrefString(TemporaryPreferences.PREF_RECEIVER_NAME, "");
        String receiverMobile = preferences.getPrefString(TemporaryPreferences.PREF_RECEIVER_MOBILE, "");
        tvReceiverProperty.setText(receiverName.trim() + " | " + receiverMobile.trim());
        mangeSetPrice();
    }

    private void mangeSetPrice() {
        switch (currMode) {
            case TAG_MODE_NORMAL:
            case TAG_MODE_REVIEW:
                tvPrice.setText(Util.getDecimalFormattedString(preferences.getPrefString(
                        TemporaryPreferences.PREF_ORDER_PRICE, getString(R.string.error_code_103))) + " ريال ");
                break;

            case TAG_MODE_FINAL:
                callApiGetOrderByOrderId();
                break;
        }
    }

    private void initList() {
        List<StructDetailFactor> dataList = new ArrayList<>();
        dataList.add(new StructDetailFactor(getString(R.string.carry_worker), preferences
                .getPrefString(TemporaryPreferences.PREF_KARGAR_BAR, "0")));
        dataList.add(new StructDetailFactor(getString(R.string.cat_worker), preferences
                .getPrefString(TemporaryPreferences.PREF_KARGAR_CHIDEMAN, "0")));
        dataList.add(new StructDetailFactor(getString(R.string.walking_origin_long), preferences
                .getPrefString(TemporaryPreferences.PREF_ORIGIN_WALKING, "0")));
        dataList.add(new StructDetailFactor(getString(R.string.walking_destination_long), preferences
                .getPrefString(TemporaryPreferences.PREF_DESTINATION_WALKING, "0")));
        dataList.add(new StructDetailFactor(getString(R.string.conut_stair_orgin), preferences
                .getPrefString(TemporaryPreferences.PREF_ORIGIN_FLOOR, "0")));
        dataList.add(new StructDetailFactor(getString(R.string.coint_stair_detination), preferences
                .getPrefString(TemporaryPreferences.PREF_DESTINATION_FLOOR, "0")));

        try {
            String rawJson = preferences.getPrefString(TemporaryPreferences.PREF_VASAYEL_HAZINEHDAR, "");
            JSONObject mainJsonObject = new JSONObject(rawJson);
            JSONArray mainArray = mainJsonObject.getJSONArray(Constants.JSON_DATA);
            for (int i = 0; i < mainArray.length(); i++) {
                dataList.add(new StructDetailFactor(
                        mainArray.getJSONObject(i).getString(Constants.JSON_SUBJECT),
                        mainArray.getJSONObject(i).getString(Constants.JSON_COUNT)));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        DetailFactorAdapter mAdapter = new DetailFactorAdapter(dataList);
        rvDetailFactor.setHasFixedSize(true);
        rvDetailFactor.setLayoutManager(new LinearLayoutManager(this));
        rvDetailFactor.setNestedScrollingEnabled(false);
        rvDetailFactor.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_factor_tv_confirm:
                manageConfirm();
                break;

            case R.id.activity_factor_tv_discount:
                manageDialogDiscount();
                break;
        }
    }

    private void manageConfirm() {

        switch (currMode) {
            case TAG_MODE_NORMAL:
                callApiAcceptOrder();
                break;

            case TAG_MODE_REVIEW:
                finish();
                break;

            case TAG_MODE_FINAL:
                callApiSendToBank();
                break;
        }
    }

    private void callApiGetOrderByOrderId() {
        tvPrice.setVisibility(View.INVISIBLE);
        pbPrice.setVisibility(View.VISIBLE);

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    pbPrice.setVisibility(View.INVISIBLE);
                    tvPrice.setVisibility(View.VISIBLE);
                    finalPrice = object.getJSONArray(Constants.JSON_DATA).getJSONObject(0)
                            .getString(Constants.JSON_PRICE);
                    tvPrice.setText(Util.getDecimalFormattedString(finalPrice) + " ريال ");
                    preferences.addPrefString(TemporaryPreferences.PREF_ORDER_PRICE, finalPrice);
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                pbPrice.setVisibility(View.INVISIBLE);
                tvPrice.setVisibility(View.VISIBLE);
                GlobalFunction.getInstance().toast(FactorActivity.this, string);
            }
        };

        String link = Constants.API_GET_ORDER_BY_ORDER_ID + preferences.
                getPrefString(TemporaryPreferences.PREF_ORDER_ID, "");
        new OkHttpHandler(this, link, eventListener)
                .send();
    }

    private void callApiAcceptOrder() {
        tvConfirm.setVisibility(View.INVISIBLE);
        pbConfirm.setVisibility(View.VISIBLE);

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    pbConfirm.setVisibility(View.INVISIBLE);
                    tvConfirm.setVisibility(View.VISIBLE);
                    preferences.addPreBool(TemporaryPreferences.PREF_IS_ACTIVE_ORDER, true);
                    startActivity(new Intent(FactorActivity.this, RequestActivity.class));
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                pbConfirm.setVisibility(View.INVISIBLE);
                tvConfirm.setVisibility(View.VISIBLE);
                GlobalFunction.getInstance().toast(FactorActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_ACCEPT_ORDER, eventListener)
                .addParam(Constants.JSON_ORDER_ID, preferences.
                        getPrefString(TemporaryPreferences.PREF_ORDER_ID, "")).send();
    }

    private void callApiSendToBank() {
        tvConfirm.setVisibility(View.INVISIBLE);
        pbConfirm.setVisibility(View.VISIBLE);
        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    pbConfirm.setVisibility(View.INVISIBLE);
                    tvConfirm.setVisibility(View.VISIBLE);

                    if (Objects.equals(object.getJSONObject(Constants.JSON_DATA)
                            .getString(Constants.JSON_CODE), Constants.PAYMENT_IS_READY)) {

                        manageApiSendToBank(object.getJSONObject(Constants.JSON_DATA)
                                .getString(Constants.JSON_TOKEN));
                    } else {
                        onError(object.getString(Constants.JSON_MESSAGE));
                    }
                } else {
                    pbConfirm.setVisibility(View.INVISIBLE);
                    tvConfirm.setVisibility(View.VISIBLE);
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                GlobalFunction.getInstance().toast(FactorActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_SEND_TO_BANK, eventListener)
                .addParam(Constants.JSON_TYPE, "mobile")
                .addParam(Constants.JSON_AMOUNT, finalPrice)
                .addParam(Constants.JSON_ORDER_ID, preferences.getPrefString(TemporaryPreferences.PREF_ORDER_ID, ""))
                .send();
    }

    private void manageApiSendToBank(String token) {
        TemporaryPreferences pref = TemporaryPreferences.getInstance();
        pref.addPrefString(TemporaryPreferences.PREF_BANK_REQUEST, TemporaryPreferences.PREF_BANK_REQUEST_PAY_COST);
        String url = Constants.API_BANK + token;
        Intent intent = new Intent(this, PayActivity.class);
        intent.putExtra(KEY_BANK_URL, url);
        startActivity(intent);
    }

    private void manageDialogDiscount() {
        new DiscountDialog(this, new DiscountDialog.DiscountDialogHandler() {
            @Override
            public void onConfirmClicked(String discountCode) {
                callApiCheckDiscountCode(discountCode);
            }

            @Override
            public void onCancelClicked() {

            }
        }).onCreateDialog();
    }

    private void callApiCheckDiscountCode(String discountCode) {
        tvDiscount.setVisibility(View.INVISIBLE);
        pbDiscount.setVisibility(View.VISIBLE);

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    pbDiscount.setVisibility(View.INVISIBLE);
                    tvDiscount.setVisibility(View.VISIBLE);
                    tvDiscount.setText(R.string.applied_discount_code);
                    tvDiscount.setEnabled(false);
                    GlobalFunction.getInstance().toast(FactorActivity.this, object.getString(Constants.JSON_MESSAGE));
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                pbDiscount.setVisibility(View.INVISIBLE);
                tvDiscount.setVisibility(View.VISIBLE);
                GlobalFunction.getInstance().toast(FactorActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_GET_CHECK_DISCOUNT_CODE, eventListener)
                .addParam(Constants.JSON_ORDER_ID, preferences.getPrefString(TemporaryPreferences.PREF_ORDER_ID, ""))
                .addParam(Constants.JSON_BON, discountCode)
                .send();
    }
}