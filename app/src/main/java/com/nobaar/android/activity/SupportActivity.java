package com.nobaar.android.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nobaar.android.R;
import com.nobaar.android.global.AppPreferences;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.network.NetworkEventListener;
import com.nobaar.android.network.OkHttpHandler;

import org.json.JSONException;
import org.json.JSONObject;


public class SupportActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etSubject, etMessage;
    private TextView tvCall;
    private ProgressBar pbSendMessage;
    private ImageView ivSendMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        initVariable();

    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        etMessage = findViewById(R.id.activity_support_et_message);
        etSubject = findViewById(R.id.activity_support_et_subject);
        tvCall = findViewById(R.id.activity_support_tv_call);
        ivSendMessage = findViewById(R.id.activity_support_iv_send_message);
        pbSendMessage = findViewById(R.id.activity_support_pb_send_message);

        tvCall.setOnClickListener(this);
        ivSendMessage.setOnClickListener(this);
        findViewById(R.id.iv_back).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_support_tv_call:

                break;
            case R.id.activity_support_iv_send_message:
                manageSendMessage();

                break;
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }

    private void manageSendMessage() {
        if (validation()) {
            pbSendMessage.setVisibility(View.VISIBLE);
            ivSendMessage.setVisibility(View.INVISIBLE);

            NetworkEventListener eventListener = new NetworkEventListener() {
                @Override
                public void onSuccess(JSONObject object) throws JSONException {
                    if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                        pbSendMessage.setVisibility(View.INVISIBLE);
                        ivSendMessage.setVisibility(View.VISIBLE);
                        etMessage.setText("");
                        etSubject.setText("");
                        GlobalFunction.getInstance().toast(SupportActivity.this, object.getString(Constants.JSON_MESSAGE));
                    } else {
                        onError(object.getString(Constants.JSON_MESSAGE));
                    }
                }

                public void onError(String string) {
                    pbSendMessage.setVisibility(View.INVISIBLE);
                    ivSendMessage.setVisibility(View.VISIBLE);

                }
            };

            new OkHttpHandler(this, Constants.API_SEND_MESSAGE, eventListener)
                    .addParam(Constants.JSON_USER_ID_LOWER_CASE, AppPreferences.getInstance().getUserId())
                    .addParam(Constants.JSON_TITLE, etSubject.getText().toString())
                    .addParam(Constants.JSON_BODY, etMessage.getText().toString())
                    .send();

        }
    }

    private boolean validation() {
        if (etSubject.getText().toString().length() == 0) {
            etSubject.setError("عنوان من نمی تواند خالی باشد.");
            return false;
        }
        if (etMessage.getText().toString().length() == 0) {
            etMessage.setError("توضیحات نمی تواند خالی باشد.");
            return false;
        }
        return true;
    }
}
