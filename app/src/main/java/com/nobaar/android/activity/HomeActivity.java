package com.nobaar.android.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appyvet.rangebar.RangeBar;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.nobaar.android.R;
import com.nobaar.android.db.StructAddressHelper;
import com.nobaar.android.global.AppPreferences;
import com.nobaar.android.global.Constants;
import com.nobaar.android.global.GlobalFunction;
import com.nobaar.android.global.TemporaryPreferences;
import com.nobaar.android.homePage.GetAddress;
import com.nobaar.android.homePage.LocationUtil;
import com.nobaar.android.homePage.ManageNavigationDrawer;

import java.util.Locale;

import static com.nobaar.android.global.Constants.NO_DATA;

public class HomeActivity extends AppCompatActivity implements OnMapReadyCallback,
        View.OnClickListener, LocationUtil.LocationUtilHandler {

    private GoogleMap mMap;

    private TextView tvOriginStairCounter, tvDestinationCounter;
    private EditText etOriginAddress, etDestinationAddress, etReceiverName, etReceiverPhoneNumber;
    private ImageView ivPinMap, ivAddBookmark, ivCurrLocation;
    private Location currLocation;
    private LatLng originLatLang, destinationLatLang;
    private boolean isUiUpdated = false;
    private AppPreferences preferences;
    private Geocoder mGeocoder;
    private View vBottomSheet;
    private BottomSheetBehavior<View> mBottomSheetBehavior;
    private LinearLayout llOriginContent, llDestinationContent;
    private int bottomSheetHeight = 0;
    private boolean isInProcess = false;
    private GetAddress.GetAddressHandler userAddressTaskListener;
    private RangeBar rbOriginWalking, rbDestinationWalking;
    private SwitchCompat scCostWithReceiver;
    private GlobalFunction function;
    private int currStateBottomSheet = BottomSheetBehavior.STATE_COLLAPSED;
    private ManageNavigationDrawer manageDrawer;
    private LocationUtil mLocationUtil;
    private Marker markerDestination;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initializer();
    }

    private void initializer() {
        manageActiveOrder();
        initMap();
        initClasses();
        initVariable();
        setOnClick();
        initBottomSheet();
    }

    private void manageActiveOrder() {
        if (TemporaryPreferences.getInstance()
                .getPrefBool(TemporaryPreferences.PREF_IS_ACTIVE_ORDER, false))
            startActivity(new Intent(this, RequestActivity.class));
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void updateUi(Location location) {

        currLocation = location;
        if (!isUiUpdated) {
            moveCamera(currLocation.getLatitude(), currLocation
                    .getLongitude(), 17);

            ivPinMap.setVisibility(View.VISIBLE);
            ivPinMap.setImageResource(R.drawable.ic_origin);
            isUiUpdated = true;

            new GetAddress(this, mGeocoder, userAddressTaskListener)
                    .execute(getFromScreenLocation());
        }
    }

    private void initClasses() {
        mLocationUtil = new LocationUtil(this, this, this);
        manageDrawer = new ManageNavigationDrawer(this, this, getWindow()
                .getDecorView().getRootView());
        mGeocoder = new Geocoder(this, new Locale("fa"));
        function = GlobalFunction.getInstance();
        function.overrideFonts(this, getWindow().getDecorView().getRootView());
    }

    private void initVariable() {
        preferences = AppPreferences.getInstance();
        tvOriginStairCounter = findViewById(R.id.activity_home_origin_tv_stairs_counter);
        tvDestinationCounter = findViewById(R.id.activity_home_destination_tv_stairs_counter);
        ivPinMap = findViewById(R.id.activity_home_iv_pin);
        ivAddBookmark = findViewById(R.id.activity_home_iv_add_bookmark);
        ivCurrLocation = findViewById(R.id.activity_home_iv_curr_location);
                vBottomSheet = findViewById(R.id.activity_home_sv_bottom_sheet);
        llOriginContent = findViewById(R.id.activity_home_ll_origin_content);
        llDestinationContent = findViewById(R.id.activity_home_ll_destination_content);
        etOriginAddress = findViewById(R.id.activity_home_et_origin_address);
        etDestinationAddress = findViewById(R.id.activity_home_et_destination_address);
        etReceiverName = findViewById(R.id.activity_home_et_receiver_name);
        etReceiverPhoneNumber = findViewById(R.id.activity_home_et_receiver_phone_number);
        rbOriginWalking = findViewById(R.id.activity_home_origin_rb_walking);
        rbDestinationWalking = findViewById(R.id.activity_main_destination_rb_walking);
        scCostWithReceiver = findViewById(R.id.activity_main_origin_sc_cost);
        ivPinMap.setVisibility(View.GONE);

        rbOriginWalking.setTickEnd(100);
        rbDestinationWalking.setTickEnd(100);
        rbOriginWalking.setSeekPinByValue(0);
        rbDestinationWalking.setSeekPinByValue(0);

        ((TextView) findViewById(R.id.activity_home_origin_tv_title)).setTypeface(Constants.iranSenseBold);
        ((TextView) findViewById(R.id.activity_home_destination_tv_title)).setTypeface(Constants.iranSenseBold);
    }

    private void setOnClick() {
        ivPinMap.setOnClickListener(this);
        ivCurrLocation.setOnClickListener(this);
        ivAddBookmark.setOnClickListener(this);
        int[] arraySetClick = new int[]{R.id.activity_home_origin_iv_stairs_counter_mines,
                R.id.activity_home_origin_iv_stairs_counter_plus,
                R.id.activity_home_destination_iv_stairs_counter_mines,
                R.id.activity_home_destination_iv_stairs_counter_plus,
                R.id.activity_home_origin_tv_confirm, R.id.activity_home_rl_credit,
                R.id.activity_home_destination_tv_confirm, R.id.include_app_bar_iv_drawer,
                R.id.activity_home_iv_bookmark, R.id.activity_home_tv_search,
                R.id.include_app_bar_iv_history, R.id.include_app_bar_fl_comment};

        for (int anArrayId : arraySetClick) {
            findViewById(anArrayId).setOnClickListener(this);
        }

        int[] arrayIdVisible = new int[]{R.id.include_app_bar_iv_drawer,
                R.id.include_app_bar_iv_history/*, R.id.include_app_bar_fl_comment*/};
        for (int anArrayIdVisible : arrayIdVisible) {
            findViewById(anArrayIdVisible).setVisibility(View.VISIBLE);
        }
    }

        private void initBottomSheet() {
            bottomSheetHeight = (int) (function.geHeightScreenSize(this) / 1.6);
            mBottomSheetBehavior = BottomSheetBehavior.from(vBottomSheet);
            mBottomSheetBehavior.setHideable(true);
            mBottomSheetBehavior.setPeekHeight((int) (bottomSheetHeight / 1.65));
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    currStateBottomSheet = newState;

                    switch (newState) {
                        case BottomSheetBehavior.STATE_DRAGGING:
                        case BottomSheetBehavior.STATE_COLLAPSED:
                        case BottomSheetBehavior.STATE_SETTLING:
                        case BottomSheetBehavior.STATE_HIDDEN:
                            if (markerDestination != null)
                                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                            break;
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });
            ViewGroup.LayoutParams params = vBottomSheet.getLayoutParams();
            params.height = bottomSheetHeight;
            vBottomSheet.setLayoutParams(params);
        }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);

        changeMapStyle();
        setCameraOnDefaultLocation();
        ivPinMap.setVisibility(View.VISIBLE);
        ivPinMap.setImageResource(R.drawable.ic_origin);

        userAddressTaskListener = new GetAddress.GetAddressHandler() {
            @Override
            public void onTaskCompleted(String response) {
                if (currStateBottomSheet != BottomSheetBehavior.STATE_EXPANDED) {
                    if (!isInProcess) {
                        etOriginAddress.setText(response);
                    } else {
                        etDestinationAddress.setText(response);
                    }
                }
            }
        };

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                setCameraChangeListener();
            }
        });
    }

    private void setCameraOnDefaultLocation() {
        LatLng tehran = new LatLng(35.6892, 51.3890);
        currLocation = new Location("");
        currLocation.setLatitude(tehran.latitude);
        currLocation.setLongitude(tehran.longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tehran, 11));
    }

    private void changeMapStyle() {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));

            if (!success) {
                Log.e("Style parsing ", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("Style parsing ", "Can't find style. Error: ", e);
        }
    }

    private void setCameraChangeListener() {
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                if (ivPinMap.getVisibility() == View.VISIBLE) {

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            new GetAddress(HomeActivity.this, mGeocoder,
                                    userAddressTaskListener)
                                    .execute(getFromScreenLocation());
                        }
                    }, 200);
                }
            }
        });
    }

    private void addOriginMarker() {
        // Add a marker in Sydney and move the camera
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(originLatLang)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_origin)));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(originLatLang, mMap.getCameraPosition().zoom));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.include_app_bar_iv_drawer:
                manageDrawer.getDrawerLayout().openDrawer(Gravity.END);
                break;

            case R.id.activity_home_iv_add_bookmark:
                manageAddBookmark();
                break;

            case R.id.activity_home_iv_curr_location:
                setUsrLocationInMap();
                break;

            case R.id.activity_home_iv_pin:
                manageOnMapPinClick();
                break;

            case R.id.activity_home_origin_iv_stairs_counter_mines:
            case R.id.activity_home_destination_iv_stairs_counter_mines:
                manageStairCounter(false);
                break;

            case R.id.activity_home_origin_iv_stairs_counter_plus:
            case R.id.activity_home_destination_iv_stairs_counter_plus:
                manageStairCounter(true);
                break;

            case R.id.activity_home_origin_tv_confirm:
            case R.id.activity_home_destination_tv_confirm:
                manageConfirmation();
                break;

            case R.id.activity_home_iv_bookmark:
                startActivityForResult(new Intent(this, BookmarkActivity.class)
                        , BookmarkActivity.REQUEST_CODE_ADDRESS_HELPER);
                break;

            case R.id.activity_home_tv_search:
                startActivityForResult(new Intent(this, SearchActivity.class)
                        , BookmarkActivity.REQUEST_CODE_ADDRESS_HELPER);
                break;

            case R.id.include_app_bar_iv_history:
                startActivity(new Intent(this, TransportationActivity.class));
                break;

            case R.id.include_app_bar_fl_comment:
                startActivity(new Intent(this, MessageActivity.class));
                break;

            case R.id.activity_home_rl_credit:
                startActivity(new Intent(this, AccountancyActivity.class));
                break;
        }
    }

    private void manageAddBookmark() {
        final StructAddressHelper structAddressHelper = new StructAddressHelper();

        LatLng mLatLang = getFromScreenLocation();

        structAddressHelper.setLatitude(String.valueOf(mLatLang.latitude));
        structAddressHelper.setLongitude(String.valueOf(mLatLang.longitude));
        new GetAddress(this, mGeocoder, new GetAddress.GetAddressHandler() {
            @Override
            public void onTaskCompleted(String response) {
                structAddressHelper.setAddress(response);
                structAddressHelper.insert();
                function.toast(HomeActivity.this, getString(R.string.addres_added));
            }
        }).execute(mLatLang);
    }

    private void setUsrLocationInMap() {
        Task locationResult = mLocationUtil.getLastLocation();

        if (locationResult != null) {
            locationResult.addOnCompleteListener(this, new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        Location mLocation = (Location) task.getResult();
                        if (mLocation == null) {
                            function.toast(HomeActivity.this,
                                    getString(R.string.error_set_user_location_in_map));
                            return;
                        }
                        moveCamera(mLocation.getLatitude(), mLocation.getLongitude(), NO_DATA);

                    } else {
                        function.toast(HomeActivity.this,
                                getString(R.string.error_set_user_location_in_map));
                    }
                }
            });
        } else {
            function.toast(HomeActivity.this,
                    getString(R.string.error_set_user_location_in_map));
        }
    }

    private void manageOnMapPinClick() {
        if (mBottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {

            if (isInProcess) {
                ivPinMap.setVisibility(View.GONE);
                destinationLatLang = getFromScreenLocation();

                addDestinationMarker();
                makeBound();
                takeLockMap();
            }
        }
        llOriginContent.setVisibility(isInProcess ? View.GONE : View.VISIBLE);
        llDestinationContent.setVisibility(isInProcess ? View.VISIBLE : View.GONE);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        new GetAddress(this, mGeocoder, userAddressTaskListener)
                .execute(getFromScreenLocation());
    }

    private void takeLockMap() {
        ivPinMap.setVisibility(View.INVISIBLE);
        ivCurrLocation.setVisibility(View.INVISIBLE);
        mMap.getUiSettings().setAllGesturesEnabled(false);
    }

    private void addDestinationMarker() {
        // Add a marker in Sydney and move the camera
        markerDestination = mMap.addMarker(new MarkerOptions().position(destinationLatLang)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destenation)));
      /*  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(destinationLatLang,
                mMap.getCameraPosition().zoom));*/
    }

    private void makeBound() {
        LatLngBounds.Builder mBoundsBuilder = new LatLngBounds.Builder();
        mBoundsBuilder.include(originLatLang);
        mBoundsBuilder.include(destinationLatLang);
        LatLngBounds bounds = mBoundsBuilder.build();
        mMap.setPadding(90, 170, 90, (int) Math.round(bottomSheetHeight / 1.17));
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,
                Math.round(0));
        mMap.animateCamera(cu);
    }

    private void manageStairCounter(boolean isPlus) {
        int counter;
        if (!isInProcess)
            counter = Integer.parseInt(tvOriginStairCounter.getText().toString());
        else
            counter = Integer.parseInt(tvDestinationCounter.getText().toString());

        if (isPlus)
            counter++;
        else if (counter > 0)
            counter--;

        if (!isInProcess)
            tvOriginStairCounter.setText(String.valueOf(counter));
        else
            tvDestinationCounter.setText(String.valueOf(counter));

    }

    private void manageConfirmation() {
        if (!isInProcess) {
            manageConfirmOrigin();
            return;
        }

        if (manageConfirmDestination())
            if (storeData())
                startActivity(new Intent(this, CompleteOrderActivity.class));
            else
                function.toast(HomeActivity.this,
                        getString(R.string.error_set_user_location_in_map));
    }

    private void manageConfirmOrigin() {
        if (etOriginAddress.getText().length() == 0) {
            function.toast(this, getString(R.string.define_origin_address));
            return;
        }

        originLatLang = getFromScreenLocation();
        addOriginMarker();
        ivPinMap.setImageResource(R.drawable.ic_destenation);
        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }
        manageViewInProcess(true);
    }

    private boolean manageConfirmDestination() {
        if (etDestinationAddress.getText().length() == 0) {
            function.toast(this, getString(R.string.define_destination_address));
            return false;
        }
        // destinationLatLang = getFromScreenLocation();
        return true;
    }

    private void manageViewInProcess(boolean status) {
        isInProcess = status;
        ivAddBookmark.setVisibility(status ? View.INVISIBLE : View.VISIBLE);
    }

    private boolean storeData() {
        if (originLatLang != null && destinationLatLang != null) {
            TemporaryPreferences preferences = TemporaryPreferences.getInstance();
            preferences.setOriginLat(originLatLang.latitude);
            preferences.setOriginLong(originLatLang.longitude);
            preferences.setDestinationLat(destinationLatLang.latitude);
            preferences.setDestinationLong(destinationLatLang.longitude);
            preferences.addPrefString(TemporaryPreferences.PREF_ADDRESS_ORIGIN, etOriginAddress
                    .getText().toString());
            preferences.addPrefString(TemporaryPreferences.PREF_ADDRESS_DESTINATION, etDestinationAddress
                    .getText().toString());
            preferences.addPrefString(TemporaryPreferences.PREF_ORIGIN_FLOOR, tvOriginStairCounter
                    .getText().toString());
            preferences.addPrefString(TemporaryPreferences.PREF_DESTINATION_FLOOR,
                    tvDestinationCounter.getText().toString());
            preferences.addPrefString(TemporaryPreferences.PREF_ORIGIN_WALKING, rbOriginWalking
                    .getRightIndex() + "");
            preferences.addPrefString(TemporaryPreferences.PREF_DESTINATION_WALKING, rbDestinationWalking
                    .getRightIndex() + "");
            preferences.addPreBool(TemporaryPreferences.PREF_PAY_WITH_RECEIVER, scCostWithReceiver
                    .isChecked());

            String receiverName = etReceiverName.getText().toString().trim();
            if (receiverName.length() == 0)
                receiverName = AppPreferences.getInstance().getUserName() + " " + AppPreferences.getInstance().getUserFamily();
            preferences.addPrefString(TemporaryPreferences.PREF_RECEIVER_NAME, receiverName);

            String receiverMobile = etReceiverPhoneNumber.getText().toString().trim();
            if (receiverMobile.length() == 0)
                receiverMobile = AppPreferences.getInstance().getUserPhoneNumber();
            preferences.addPrefString(TemporaryPreferences.PREF_RECEIVER_MOBILE, receiverMobile);

            return true;
        }
        return false;
    }

    private LatLng getFromScreenLocation() {
        int y = (int) ivPinMap.getY();
        ivPinMap.measure(0, 0);
        y += ivPinMap.getMeasuredHeight();
        return mMap.getProjection().fromScreenLocation(
                new Point(Math.round(ivPinMap.getX())
                        , Math.round(y)));
    }

    private void moveCamera(double lat, double lang, float zoom) {
        if (zoom == NO_DATA)
            zoom = mMap.getCameraPosition().zoom;

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(lat, lang), zoom));
    }

    @Override
    public void onBackPressed() {

        if (!manageComponent())
            return;

        // manage Destination Marker in back
        if (markerDestination != null) {
            ivPinMap.setImageResource(R.drawable.ic_destenation);
            ivPinMap.setVisibility(View.VISIBLE);
            ivCurrLocation.setVisibility(View.VISIBLE);
            mMap.setPadding(0, 0, 0, 0);
            mMap.getUiSettings().setAllGesturesEnabled(true);
            moveCamera(destinationLatLang.latitude, destinationLatLang.longitude, NO_DATA);
            markerDestination.remove();
            markerDestination = null;
            manageComponent();

            return;
        }

        // manage Origin Marker in back
        if (isInProcess) {
            ivPinMap.setImageResource(R.drawable.ic_origin);
            ivPinMap.setVisibility(View.VISIBLE);
            moveCamera(originLatLang.latitude, originLatLang.longitude, NO_DATA);
            mMap.clear();
            manageViewInProcess(false);
            mMap.getUiSettings().setAllGesturesEnabled(true);
            mMap.setPadding(0, 0, 0, 0);

            return;
        }


        super.onBackPressed();
    }

    private boolean manageComponent() {

        //manage Navigation Drawer in back
        if (manageDrawer.getDrawerLayout().isDrawerOpen(Gravity.END)) {
            manageDrawer.getDrawerLayout().closeDrawers();
            return false;
        }

        //manage Navigation Drawer in back
        if (markerDestination == null &&
                mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED ||
                mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {

            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            return false;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BookmarkActivity.REQUEST_CODE_ADDRESS_HELPER && data != null) {
            moveCamera(Double.valueOf(data.getStringExtra(BookmarkActivity.TAG_LAT)),
                    Double.valueOf(data.getStringExtra(BookmarkActivity.TAG_LNG)),
                    NO_DATA);
        }

    }

}
