package com.nobaar.android.activity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.nobaar.android.Application;
import com.nobaar.android.R;
import com.nobaar.android.adapter.AddressHelperAdapter;
import com.nobaar.android.db.StructAddressHelper;
import com.nobaar.android.global.GlobalFunction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SearchActivity extends AppCompatActivity implements AddressHelperAdapter
        .AddressHelperAdapterHandler, View.OnClickListener {
    public static final int REQUEST_CODE_BOOKMARK_LOCATION = 30;
    public static final String TAG_ADDRESS = "address";
    public static final String TAG_LAT = "lat";
    public static final String TAG_LNG = "lng";
    private RecyclerView mRecyclerView;
    private AddressHelperAdapter mAdapter;
    private List<StructAddressHelper> dataList = new ArrayList<>();
    private Geocoder mGeocoder;
    private EditText etSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initVariable();
        initList();
        initHeader();
    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        GlobalFunction.getInstance().openKeyboard(this);
        mGeocoder = new Geocoder(this, new Locale("fa"));
        mRecyclerView = findViewById(R.id.activity_search_recycler);
        etSearch = findViewById(R.id.activity_search_et_search);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                new GetAddressInfoName().execute(" استان تهران " + s.toString());

           /*     GetAddress(s.toString());*/
            }
        });
    }

    private void initHeader() {
        ((TextView) findViewById(R.id.include_app_bar_tv_page_name)).setText(R.string.search);
        ImageView ivBack = findViewById(R.id.include_app_bar_iv_back);
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setOnClickListener(this);
    }

    private void initList() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        dataList = new ArrayList<>();
        mAdapter = new AddressHelperAdapter(this, dataList)
                .setDisableRemoveIcon(false);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemAddressClicked(int position) {
        Intent intent = new Intent();
        //   intent.putExtra(ContactsFragment.TAG_ADDRESS, s);
        intent.putExtra(TAG_LAT, dataList.get(position).getLatitude());
        intent.putExtra(TAG_LNG, dataList.get(position).getLongitude());
        setResult(REQUEST_CODE_BOOKMARK_LOCATION, intent);
        GlobalFunction.getInstance().closeKeyboard(this);
        finish();
    }

    @Override
    public void onItemRemoveBookmarkClicked(int position) {
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(0);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.include_app_bar_iv_back:
                GlobalFunction.getInstance().closeKeyboard(this);
                onBackPressed();

                break;

        }
    }

    private class GetAddressInfoName extends AsyncTask<String, Void, List<Address>> {
        @Override
        protected void onPreExecute() {
            if (!Application.getInstance().isConnectionAnyInternet()) {
                startActivity(new Intent(SearchActivity.this, NoInternetConnectionActivity.class));
                return;
            }
            super.onPreExecute();
        }

        @Override
        protected List<Address> doInBackground(String... strings) {
            List<Address> a = new ArrayList<>();
            try {
                String locationName = strings[0];
                a = mGeocoder.getFromLocationName(locationName, 30);

            } catch (IOException e) {
                e.printStackTrace();
            }

            return a;
        }

        @Override
        protected void onPostExecute(List<Address> a) {
            super.onPostExecute(a);
            dataList.clear();
            for (int i = 0; i < a.size(); i++) {
                dataList.add(new StructAddressHelper(a.get(i).getAddressLine(0),
                        String.valueOf(a.get(i).getLatitude()),
                        String.valueOf(a.get(i).getLongitude())));
            }
            mAdapter.notifyDataSetChanged();
        }
    }

}
